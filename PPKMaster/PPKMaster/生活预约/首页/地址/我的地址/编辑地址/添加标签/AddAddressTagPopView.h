//
//  AddAddressTagPopView.h
//  ZZR
//
//  Created by null on 2021/3/19.
//  Copyright © 2021 null. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AddAddressTagPopView : UIView
@property (nonatomic ,copy) void(^textFieldEnd)(NSString *passWord);

@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIButton *addBtn;

@end

NS_ASSUME_NONNULL_END
