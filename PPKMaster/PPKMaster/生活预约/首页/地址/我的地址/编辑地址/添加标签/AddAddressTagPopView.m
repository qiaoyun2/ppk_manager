//
//  AddAddressTagPopView.m
//  ZZR
//
//  Created by null on 2021/3/19.
//  Copyright © 2021 null. All rights reserved.
//

#import "AddAddressTagPopView.h"

@implementation AddAddressTagPopView

-(void)awakeFromNib{
    [super awakeFromNib];
    //修改占位符文字颜色
//    [self.textField setValue:RGBA(255, 255, 255, 0.8) forKeyPath:@"placeholderLabel.textColor"];
//    self.textField.tintColor = [UIColor whiteColor];      //设置光标颜色
    self.textField.jk_maxLength = 5;
}

- (IBAction)addBtnDidClick:(id)sender {
    
    if (_textField.text.length<1) {
        [LJTools showTextHud:@"请输入内容"];
        return;
    }
    [self endEditing:YES];
    if (![_textField.text isEqualToString:@""]) {
        if (_textFieldEnd) {
            _textFieldEnd(_textField.text);
        }
    }
}

- (IBAction)textFieldDidChange:(id)sender {
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
