//
//  EditAddressController.h
//  ZZR
//
//  Created by null on 2020/12/3.
//

#import "BaseViewController.h"
#import "AddressModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface EditAddressController : BaseViewController

@property (nonatomic, assign) BOOL isNew;

@property (nonatomic, strong) AddressModel *model;

@property (nonatomic, copy) void(^onDeleteAddress)(AddressModel *model);
@property (nonatomic, copy) void(^onEditAddress)(AddressModel *model);
@property (nonatomic, copy) void(^onAddAddress)(void);

@end

NS_ASSUME_NONNULL_END
