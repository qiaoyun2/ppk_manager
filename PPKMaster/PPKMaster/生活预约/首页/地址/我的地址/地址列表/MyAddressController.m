//
//  MyAddressController.m
//  ZZR
//
//  Created by null on 2021/5/10.
//

#import "MyAddressController.h"
#import "MyAddressCell.h"
#import "EditAddressController.h"
@interface MyAddressController ()<MyAddressCellDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation MyAddressController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self showNavBarBottomLine];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self getListInfo];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"地址管理";
    [self initUI];
}

#pragma mark – UI
-(void)initUI{
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.estimatedRowHeight = 0.5;
    UIView * view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0)];
    view.backgroundColor = [UIColor whiteColor];
    _tableView.tableFooterView = view;
    
    [_tableView registerNib:[UINib nibWithNibName:@"MyAddressCell" bundle:nil] forCellReuseIdentifier:@"MyAddressCell"];
//    WeakSelf
//    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^(void) {
//        NSLog(@"下拉刷新");
//        [weakSelf refresh];
//    }];
//    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//        NSLog(@"上拉加载更多");
//        [weakSelf loadMore];
//    }];
}
#pragma mark – Network
-(void)getListInfo{
    
//    return;
//    kPostUserAddressList
    [NetworkingTool postWithUrl:@"" params:@{} success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        [self.dataArray removeAllObjects];
        [self.tableView reloadData];
        if ([responseObject[@"code"]intValue]==SUCCESS) {
            NSArray * dataList = responseObject[@"data"];
            for (NSDictionary * dict in dataList) {
                AddressModel * model = [AddressModel mj_objectWithKeyValues:dict];
                [self.dataArray addObject:model];
            }
            
            [self.tableView reloadData];
        }else{
            [LJTools showNOHud:responseObject[@"msg"] delay:1];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}

#pragma mark – Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger index = self.dataArray.count;
    return index;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MyAddressCell * cell = [tableView dequeueReusableCellWithIdentifier:@"MyAddressCell" forIndexPath:indexPath];
    cell.selectionStyle = 0;
    AddressModel * model = self.dataArray[indexPath.row];
    cell.model = model;
    cell.delegate = self;
    return cell;
}

- (void)MyAddressCell:(MyAddressCell *)cell didSelectBtn:(UIButton *)sender{
    NSIndexPath * indexPath = [_tableView indexPathForCell:cell];
    AddressModel * model = self.dataArray[indexPath.row];
    if (sender.tag == 1) {
        [NetworkingTool postWithUrl:@"" params:@{@"address_id":model.address_id} success:^(NSURLSessionDataTask *task, id responseObject) {
            [LJTools hideHud];
            if ([responseObject[@"code"]intValue]==SUCCESS) {
                [self getListInfo];
            }else{
                [LJTools showNOHud:responseObject[@"msg"] delay:1];
            }
        } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
            [LJTools showNOHud:RequestServerError delay:1.0];

        } IsNeedHub:YES];
    }
    else if(sender.tag == 2){
        EditAddressController * vc = [EditAddressController new];
        vc.model = model;
        [self.navigationController pushViewController:vc animated:YES];
        WeakSelf
        [vc setOnEditAddress:^(AddressModel * _Nonnull result) {
            [weakSelf getListInfo];
        }];
    }
    else{

    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.isorderJump == YES){
        AddressModel * model = self.dataArray[indexPath.row];
        if (self.selectAddressBlock){
            self.selectAddressBlock(model,nil);
        }
        [self.navigationController popViewControllerAnimated:true];
    }
}
#pragma mark - Function
#pragma mark – XibFunction
- (IBAction)addAddress:(id)sender {
    EditAddressController * vc = [EditAddressController new];
    vc.isNew = YES;
    [self.navigationController pushViewController:vc animated:YES];
    WeakSelf
    [vc setOnAddAddress:^{
        [weakSelf getListInfo];
    }];
}
#pragma mark – lazy load

@end
