//
//  MyAddressCell.m
//  ZZR
//
//  Created by null on 2020/9/15.
//  Copyright © 2020 null. All rights reserved.
//

#import "MyAddressCell.h"


@implementation MyAddressCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code


}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setModel:(AddressModel *)model{
    _model = model;
    NSString * sex = model.sex.intValue == 1?@"(男士)":@"(女士)";
    _nameLabel.text = [NSString stringWithFormat:@"%@%@",model.name,sex];
    _phoneLabel.text = model.mobile;
    _addressLabel.text = [NSString stringWithFormat:@"%@%@",model.city,model.address];
    
    if ([model.is_default isEqualToString:@"1"]) {
        _defaultBtn.selected = YES;
    }else{
        _defaultBtn.selected = NO;
    }
    if (model.label.length>0) {
        [self.defaultButton setTitle:model.label forState:UIControlStateNormal];
        self.defaultButton.hidden = NO;
    }else{
        self.defaultButton.hidden = YES;
    }
}

- (IBAction)btnClick:(UIButton *)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(MyAddressCell:didSelectBtn:)]) {
        [_delegate MyAddressCell:self didSelectBtn:sender];
    }
}

@end
