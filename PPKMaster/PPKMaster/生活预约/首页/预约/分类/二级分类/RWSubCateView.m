//
//  RWSubCateView.m
//  PPK
//
//  Created by null on 2022/4/12.
//

#import "RWSubCateView.h"
#import "RWSubCateCell.h"

@interface RWSubCateView ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *cateNameLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewTrailing;


@end

@implementation RWSubCateView

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.topViewHeight.constant = StatusHight + 43;
    self.contentViewWidth.constant = SCREEN_WIDTH*0.7;
    self.contentViewTrailing.constant = - self.contentViewWidth.constant;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}

#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RWSubCateCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RWSubCateCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"RWSubCateCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark - Function
- (void)show
{
    self.hidden = NO;
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.2 animations:^{
        self.backgroundColor = RGBA(0, 0, 0, 0.4);
        self.contentViewTrailing.constant = 0;
        [self layoutIfNeeded];
    }];
}

- (void)dismiss
{
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.2 animations:^{
        self.backgroundColor = RGBA(0, 0, 0, 0);
        self.contentViewTrailing.constant = - self.contentViewWidth.constant;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.hidden = YES;
    }];
}

- (IBAction)closeButtonAction:(id)sender {
    [self dismiss];
}


@end
