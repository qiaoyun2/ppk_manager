//
//  RWSubCateView.h
//  PPK
//
//  Created by null on 2022/4/12.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RWSubCateView : UIView

@property (nonatomic, copy) void(^didSelectBlock)(NSString *selectedStr);

- (void)show;
- (void)dismiss;

@end

NS_ASSUME_NONNULL_END
