//
//  RWReserveController.m
//  PPK
//
//  Created by null on 2022/4/11.
//

#import "RWReserveController.h"
#import <HXPhotoPicker/HXPhotoPicker.h>
#import "HXAssetManager.h"
#import "UploadManager.h"
#import "RWCateController.h"
#import "RWSubCateView.h"
#import "RWReserveResultController.h"

@interface RWReserveController ()<HXPhotoViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *cateButton;
@property (weak, nonatomic) IBOutlet UIButton *subCateButton;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *mobileLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UITextField *numField;
@property (weak, nonatomic) IBOutlet UITextField *weightField;

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet HXPhotoView *photoView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *photoViewHeight;

@property (weak, nonatomic) IBOutlet UILabel *noticeLabel;
@property (weak, nonatomic) IBOutlet UIButton *selectButton;

@property (nonatomic, assign) BOOL isVideo;
@property (nonatomic, strong) NSMutableArray<UIImage *> *photos;
@property (nonatomic, strong) HXPhotoModel *videoModel;
@property (nonatomic, strong) NSString *videoPath;
@property (nonatomic, strong) NSMutableArray *imagePathArray;
@property (strong, nonatomic) HXPhotoManager *manager;

@property (strong, nonatomic) RWSubCateView *subCateView;



@end

@implementation RWReserveController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"预约";
    self.textView.jk_placeHolderTextView.text = @"请输入备注内容...";

    NSString *string = @"回收须知：所有用户在下单时需要支付￥50作为订单保证金，本金额只作为维修员上门时的路费（需用户确认到达后支付），师傅确认出发前双方均可无责退单，师傅出发后需要用户向平台申请退款（需用户确认到达后支付）";
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString:string];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:5];
    [attriString addAttributes:@{NSForegroundColorAttributeName: MainColor} range:NSMakeRange(0, 5)];
    [attriString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [string length])];
    [self.noticeLabel setAttributedText:attriString];
    
    self.photos = [NSMutableArray array];
    self.imagePathArray = [NSMutableArray array];
    
    self.photoView.spacing = 10.f;
    self.photoView.delegate = self;
    self.photoView.deleteCellShowAlert = YES;
    self.photoView.outerCamera = YES;
    self.photoView.previewShowDeleteButton = YES;
    self.photoView.addImageName = @"组 50938";
    self.photoView.lineCount = 4;
    self.photoView.manager = self.manager;
}

#pragma mark - Network
- (void)requestForUploadVideo
{
    NSData *data = [NSData dataWithContentsOfURL:self.videoModel.videoURL];
    WeakSelf
    [UploadManager uploadWithVideoData:data block:^(NSString * _Nonnull videoUrl) {
        NSLog(@"%@",videoUrl);
        weakSelf.videoPath = videoUrl;
    }];
}

- (void)requestForUploadImages
{
    WeakSelf
    [UploadManager uploadImageArray:self.photos block:^(NSString * _Nonnull ids, NSString * _Nonnull imageUrl) {
        NSArray *array = [imageUrl componentsSeparatedByString:@","];
        [weakSelf.imagePathArray addObjectsFromArray:array];
    }];
}


#pragma mark - HXPhotoViewDelegate
- (void)photoView:(HXPhotoView *)photoView changeComplete:(NSArray<HXPhotoModel *> *)allList photos:(NSArray<HXPhotoModel *> *)photos videos:(NSArray<HXPhotoModel *> *)videos original:(BOOL)isOriginal {

    if (videos.count>0) {
        self.isVideo = YES;
        self.videoModel = [videos firstObject];
        [self.photos removeAllObjects];
        [self.photos addObject:self.videoModel.previewPhoto];
        [self requestForUploadImages];
        [self requestForUploadVideo];
        NSLog(@"%@",self.videoModel.videoURL);
    }
    else {
        self.isVideo = NO;
        self.videoModel = nil;
        [self.photos removeAllObjects];
        for (HXPhotoModel *model in photos) {
            [self.photos addObject:[HXAssetManager originImageForAsset:model.asset]];
        }
        [self requestForUploadImages];
    }
//    self.selectList = allList;
}
// 拿到view变化的高度
- (void)photoView:(HXPhotoView *)photoView updateFrame:(CGRect)frame {
    self.photoViewHeight.constant = frame.size.height;
}


#pragma mark - XibFunction
/// 地址
- (IBAction)addressViewTap:(id)sender {
    
}

/// 预约时间
- (IBAction)timeViewTap:(id)sender {
    
}

/// 一级分类
- (IBAction)cateButtonAction:(id)sender {
    RWCateController *vc = [[RWCateController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

/// 二级分类
- (IBAction)subCateButtonAction:(id)sender {
    if (!self.subCateView) {
        self.subCateView = [[[NSBundle mainBundle] loadNibNamed:@"RWSubCateView" owner:nil options:nil] firstObject];
        self.subCateView.frame = [UIScreen mainScreen].bounds;
        [[UIApplication sharedApplication].keyWindow addSubview:self.subCateView];
    }
    [self.subCateView show];
}

/// 勾选按钮
- (IBAction)selectButtonAction:(UIButton *)sender {
    sender.selected = !sender.selected;
}

- (IBAction)agreementButtonAction:(UIButton *)sender {
    
}

/// 预约
- (IBAction)reservationButtonAction:(UIButton *)sender {
    RWReserveResultController *vc = [[RWReserveResultController alloc] init];
    vc.tipLabel.text = @"预约申请已提交";
    vc.subTipLabel.text = @"耐心等待回收员上门回收";
    [vc.backButton setTitle:@"返回首页" forState:UIControlStateNormal];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Lazy Load
- (HXPhotoManager *)manager {
    if (!_manager) {
        _manager = [[HXPhotoManager alloc] initWithType:HXPhotoManagerSelectedTypePhoto];
        _manager.configuration.type = HXConfigurationTypeWXChat;
        _manager.configuration.singleSelected = NO;
        _manager.configuration.lookGifPhoto = NO;
        _manager.configuration.useWxPhotoEdit = YES;
        _manager.configuration.selectTogether=NO;
        _manager.configuration.photoEditConfigur.onlyCliping = YES;
        _manager.configuration.showOriginalBytes = NO;
        _manager.configuration.videoMaxNum=1;
        _manager.configuration.photoMaxNum=9;
        _manager.configuration.requestImageAfterFinishingSelection = YES;
        _manager.configuration.photoEditConfigur.aspectRatio = HXPhotoEditAspectRatioType_None;
    }
    return _manager;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
