//
//  URLHeader.h
//  HOOLA
//
//  Created by null on 2018/9/18.
//  Copyright © 2018 null. All rights reserved.
//

#ifndef URLHeader_h
#define URLHeader_h

#define SUCCESS  1
#define TokenError  401
#define RequestServerError @"您的网络不稳定，请稍后重试"

#define kDomainUrl @"https://hello.pinpinkan.vip/pinpinkan-master/api/"
#define kServerUrl [NSString stringWithFormat:@"%@v1/", kDomainUrl]
#define kServerUrlV2 [NSString stringWithFormat:@"%@v2/", kDomainUrl]

#define kBaseImageUrl @"https://pinpinkans.oss-cn-hangzhou.aliyuncs.com/"

#define kImageUrl(url) [url hasPrefix:@"http"] ? [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] : [NSString stringWithFormat:@"%@%@",kBaseImageUrl,[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]]


#pragma mark ===== 通用 =====
/** 文件上传 **/
#define kUploadFileURL [NSString stringWithFormat:@"%@common/uploadImagesAli", kServerUrl]
/** 发送验证码  注册:register； 登录：login; 忘记密码：forget； 绑定微信qq:bindThird **/
#define kGetCodeURL [NSString stringWithFormat:@"%@sms/aliSend", kServerUrl]
/** 获取配置信息  registerAgreement：注册协议； privacyPolicy：隐私政策l; logo:logo; companyPicture:公司图片; personalPicture:个人图片; officialMobile:官方电话**/
#define kConfigNameURL [NSString stringWithFormat:@"%@config/config/queryValueByConfigName", kServerUrl]
/** 留言建议 **/
#define kFeedbackURL [NSString stringWithFormat:@"%@marketFeedback/add",kServerUrl]

#pragma mark ===== 登录注册 =====
/** 注册 **/
#define kRegisterURL [NSString stringWithFormat:@"%user/register", kServerUrl]
/** 账号密码登录 **/
#define kMobileLoginURL [NSString stringWithFormat:@"%@user/login", kServerUrl]
/** 手机验证码登录 **/
#define kCodeLoginURL [NSString stringWithFormat:@"%@user/loginOrRegister", kServerUrl]
/** 忘记密码 **/
#define kForgetPwdURL [NSString stringWithFormat:@"%@user/forgetPwd", kServerUrl]
/** qq、微信一键登录 **/
#define kThirdLoginURL [NSString stringWithFormat:@"%@user/mobileThirdLogin", kServerUrl]
/** qq、微信绑定手机号 **/
#define kThirdBindURL [NSString stringWithFormat:@"%@user/bindOpenId", kServerUrl]

#pragma mark ===== 会员 =====
/** 会员类型**/
#define kMasterVipTypeURL [NSString stringWithFormat:@"%@masterVipType/masterVipType/list", kServerUrl]
/** 购买会员支付**/
#define kMasterBuyVipURL [NSString stringWithFormat:@"%@masterUserVipRecord/pay", kServerUrl]
/** 购买记录**/
#define kMasterPayLogURL [NSString stringWithFormat:@"%@masterUserVipRecord/list", kServerUrl]
/** 内购**/
#define kInAppPurchaseURL [NSString stringWithFormat:@"%@vipRecharge/applePayMaster", kServerUrl]

#pragma mark ===== 我的 =====
/** 获取用户信息 **/
#define kGetUserInfoURL [NSString stringWithFormat:@"%@user/queryUser", kServerUrl]
/** 修改用户信息 **/
#define kEditUserInfoURL [NSString stringWithFormat:@"%@user/modifyUser", kServerUrl]
/** 师傅认证 **/
#define kAuthenticationMasterURL [NSString stringWithFormat:@"%@user/authenticationMaster", kServerUrl]

/** 我的评价列表 **/
#define kRWCommentListURL [NSString stringWithFormat:@"%@recovery/recoveryOrderComments/list", kServerUrl]

/** 师傅端意见反馈类型表 **/
#define kFeedBackTypeListURL [NSString stringWithFormat:@"%@masterFeedbackType/masterFeedbackType/list", kServerUrl]
/** 添加意见反馈 **/
#define kAddFeedBackURL [NSString stringWithFormat:@"%@masterFeedback/masterFeedback/add", kServerUrl]
/** 我的反馈记录 **/
#define kFeedBackLogURL [NSString stringWithFormat:@"%@masterFeedback/masterFeedback/list", kServerUrl]

/** 旧密码修改密码 **/
#define kChangePwdByOldURL [NSString stringWithFormat:@"%@user/modifyPwd", kServerUrl]
/** 验证码更改密码-验证（下一步） **/
#define kChangePwdByCodeFirstURL [NSString stringWithFormat:@"%@user/verifyChangePwd", kServerUrl]
/** 验证码更改密码-更改  **/
#define kChangePwdByCodeSecondURL [NSString stringWithFormat:@"%@user/changePwd", kServerUrl]
/** 修改手机号第一步（验证手机号）  **/
#define kChangeMobileFirstURL [NSString stringWithFormat:@"%@user/verifyMobile", kServerUrl]
/** 修改手机号第二步  **/
#define kChangeMobileSecondURL [NSString stringWithFormat:@"%@user/changeMobile", kServerUrl]
/** 注销账号 **/
#define kDelUserURL [NSString stringWithFormat:@"%@user/delUser", kServerUrl]
/** 是否设置支付密码 **/
#define kIsPayPasswordURL [NSString stringWithFormat:@"%@user/account/isPayPassword", kServerUrl]
/** 支付密码是否正确 **/
#define kCheckPayPasswordURL [NSString stringWithFormat:@"%@user/account/checkPayPassword", kServerUrl]
/** 重置支付密码 **/
#define kResetPayPasswordURL [NSString stringWithFormat:@"%@user/account/resetPayPassword", kServerUrl]
/** 验证码-设置支付密码 **/
#define kSetPayPasswordURL [NSString stringWithFormat:@"%@user/account/setPayPassword", kServerUrl]

/** 绑定微信、支付宝 **/
#define kBindReceivablesInfoURL [NSString stringWithFormat:@"%@user/account/bindReceivablesInfo", kServerUrl]
/** 获取绑定账号信息 **/
#define kGetBindInfoURL [NSString stringWithFormat:@"%@user/account/getBindInfo", kServerUrl]
/** 我的钱包 **/
#define kMyWalletURL [NSString stringWithFormat:@"%@user/accountBill/myWallet", kServerUrl]
/** 流水记录 **/
#define kFlowRecordURL [NSString stringWithFormat:@"%@user/accountBill/queryPage", kServerUrl]
/** 账户提现申请 **/
#define kWithdrawApplyURL [NSString stringWithFormat:@"%@withdraw/withdrawApply", kServerUrl]
/** 提现记录 **/
#define kWithdrawRecordURL [NSString stringWithFormat:@"%@withdraw/queryWithdraw", kServerUrl]

#pragma mark ===== 生活预约大厅 =====
/** 大厅订单列表 **/
#define kRWHallOrderURL [NSString stringWithFormat:@"%@recovery/recoveryOrder/list", kServerUrl]
/** 首页-待接订单详情 **/
#define kRWHallOrderDetailURL [NSString stringWithFormat:@"%@recovery/recoveryOrder/queryHomeOrderById", kServerUrl]
/** 回收订单-接单 **/
#define kRWTakeOrderURL [NSString stringWithFormat:@"%@recovery/recoveryOrder/receive", kServerUrl]
/** 我的订单列表 **/
#define kRWOrderListURL [NSString stringWithFormat:@"%@recovery/recoveryOrder/myOrderList", kServerUrl]
/** 回收订单-取消 **/
#define kRWCancelOrderURL [NSString stringWithFormat:@"%@recovery/recoveryOrder/cancel", kServerUrl]
/** 回收订单-完成订单 **/
#define kRWCompleteOrderURL [NSString stringWithFormat:@"%@recovery/recoveryOrder/completeOrder", kServerUrl]
/** 回收订单-删除订单 **/
#define kRWDeleteOrderURL [NSString stringWithFormat:@"%@recovery/recoveryOrder/deleteOrder", kServerUrl]
/** 我的订单详情 **/
#define kRWOrderDetailURL [NSString stringWithFormat:@"%@recovery/recoveryOrder/queryMyOrderById", kServerUrl]
/** 取消原因列表查询 **/
#define kRWCancelReasonURL [NSString stringWithFormat:@"%@recoveryOrderCancel/list", kServerUrl]

#pragma mark ===== 评价列表 =====



#endif /* URLHeader_h */
