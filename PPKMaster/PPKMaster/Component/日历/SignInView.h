//
//  SignInView.h
//  FateIsYou
//
//  Created by 王巧云 on 2021/9/27.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SignInView : UITableViewCell
///
+(SignInView *)title:(NSString *)title show:(void(^)(NSString *start,NSString *end))block;
/// 重新初始化
+ (void)reinitialize;
@end

NS_ASSUME_NONNULL_END
