//
//  GuideHeader.h
//  phonelive
//
//  Created by null on 2019/5/16.
//  Copyright © 2019 ebz. All rights reserved.
//

#ifndef GuideHeader_h
#define GuideHeader_h

// 消失动画（默认直接消失、向四周放大淡化消失、向中间缩小淡化消失）
typedef NS_ENUM(NSInteger, GuideAnimationType)
{
    /// 消失动画-直接消失，默认
    GuideAnimationTypeDefault = 0,
    
    /// 消失动画-向四周放大淡化消失
    GuideAnimationTypeZoomIn = 1,
    
    /// 消失动画-向中间缩小淡化消失
    GuideAnimationTypeZoomOut = 2
};

// 引导页视图类型（默认图片轮播、动图、视频）
typedef NS_ENUM (NSInteger, GuideViewType)
{
    /// 引导页视图类型-图片轮播，默认
    GuideViewTypeDefault = 0,
    
    /// 引导页视图类型-动图
    GuideViewTypeGif = 0,
    
    /// 引导页视图类型-视频
    GuideViewTypeVideo = 0,
};
#endif /* GuideHeader_h */
