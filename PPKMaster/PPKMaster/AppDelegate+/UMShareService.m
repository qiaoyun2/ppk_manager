//
//  UMShareService.m
//  ZZR
//
//  Created by null on 2019/8/27.
//  Copyright © 2019 null. All rights reserved.
//

#import "UMShareService.h"
#import <UMShare/UMShare.h>
#import <UMCommon/UMCommon.h>

@implementation UMShareService

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
        // 打开调试日志
    [[UMSocialManager defaultManager] openLog:YES];
    // 设置友盟appKey
    [UMConfigure initWithAppkey:UMAppKey channel:@"App Store"];
    /* 设置微信的appKey和appSecret */
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_WechatSession appKey:WechatAppKey appSecret:WechatAppSecret redirectURL:WeChatRedirectURL];
    // 配置QQ
//    [[UMSocialManager defaultManager] setPlaform:(UMSocialPlatformType_QQ) appKey:QQAppKey appSecret:QQAppSecrect redirectURL:QQRedirectURL];
//    // 配置Sina
//    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_Sina
//         appKey:SinaAppKey
//      appSecret:SinaAppSecrect
//    redirectURL:SinaRedirectURL];
    return YES;
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options{
    
    return [[UMSocialManager defaultManager]  handleOpenURL:url options:options];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    
    return [[UMSocialManager defaultManager] handleOpenURL:url sourceApplication:sourceApplication annotation:annotation];;
}

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void(^)(NSArray<id<UIUserActivityRestoring>> * __nullable restorableObjects))restorationHandler {
    //NSURL *url = userActivity.webpageURL;
    [[UMSocialManager defaultManager] handleUniversalLink:userActivity options:nil];
    return YES;
}

@end
