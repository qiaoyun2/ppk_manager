//
//  AppDelegate+CheckNetworkState.h
//  yuejiu
//
//  Created by    on 2017/11/1.
//  Copyright © 2017年   . All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (CheckNetworkState)

/**监听网络状态*/
- (void)ml_checkNetworkState;

@end
