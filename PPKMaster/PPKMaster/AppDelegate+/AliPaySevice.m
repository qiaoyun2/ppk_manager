//
//  AliPaySevice.m
//  ZZR
//
//  Created by null on 2019/8/17.
//  Copyright © 2019 null. All rights reserved.
//

#import "AliPaySevice.h"

@implementation AliPaySevice

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options{

    if ([url.host isEqualToString:@"safepay"]) {
        [self pay:url];
    }
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    
    if ([url.host isEqualToString:@"safepay"]) {
        [self pay:url];
        
    }
    return YES;
}


- (void)pay:(NSURL *)url
{
    // 支付跳转支付宝钱包进行支付，处理支付结果
    [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
        NSLog(@"-------%@", resultDic);
        NSString *resultStatus = [resultDic objectForKey:@"resultStatus"];
        if ([resultStatus isEqualToString:@"9000"])
        {
            [LJTools showText:@"购买成功" delay:1.5];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"PaySuccess" object:nil userInfo:nil];
//          NSString *state = [[NSUserDefaults standardUserDefaults] objectForKey:@"vipPay"];
//            if ([state intValue]==1) {
//                PaySuccessfulVC *vc=[[PaySuccessfulVC alloc] init];
//                vc.isVip=1;
//                vc.payType=@"支付宝支付";
//    //            [self getUserInfo];
//                [[LJTools topViewController].navigationController pushViewController:vc animated:YES];
//            }else{
//                PaySuccessfulVC *vc=[[PaySuccessfulVC alloc] init];
//                vc.payType=@"支付宝支付";
//                [[LJTools topViewController].navigationController pushViewController:vc animated:YES];
//            }

        }else{
            [LJTools showText:@"支付失败" delay:1.5];
        }
    }];
    // 授权跳转支付宝钱包进行支付，处理支付结果
    [[AlipaySDK defaultService] processAuth_V2Result:url standbyCallback:^(NSDictionary *resultDic) {
        NSLog(@"-------%@", resultDic);
    }];;
}

@end
