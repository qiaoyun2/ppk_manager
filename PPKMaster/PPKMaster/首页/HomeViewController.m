//
//  HomeViewController.m
//  PPKMaster
//
//  Created by null on 2022/4/14.
//

#import "HomeViewController.h"
#import "GrabOrderCell.h"
#import "GrabOrderDetailController.h"
#import "PersonCertificationController.h"
#import "CityViewController.h"
#import "SDCityModel.h"

@interface HomeViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewHeight;
@property (weak, nonatomic) IBOutlet UIView *certificationView;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;

@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) NSString *city;


@end

@implementation HomeViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    if ([LJTools islogin]) {
        if (!self.certificationView.hidden) {
            [self requestForUserInfo];
        }else {
            [self refresh];
        }
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = MainColor;
    self.topViewHeight.constant = NavAndStatusHight;
    if ([LJTools getAppDelegate].city.length) {
        self.city = [LJTools getAppDelegate].city;
        self.cityLabel.text = self.city;
    }else {
        self.city = @"";
    }
    WeakSelf
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^(void) {
        [weakSelf refresh];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMore];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationUpdataSuccess) name:LocationChangeSuccess object:nil];

}

#pragma mark - Network
- (void)refresh {
    _page = 1;
    [self getDataArrayFromServerIsRefresh:YES];
}
- (void)loadMore {
    _page++;
    [self getDataArrayFromServerIsRefresh:NO];
}

- (void)getDataArrayFromServerIsRefresh:(BOOL)isRefresh {
    if (!self.city.length) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        return;
    }
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
    params[@"pageNo"] = @(_page);
    params[@"pageSize"] = @(10);
    params[@"longitude"] = @([LJTools getAppDelegate].longitude); // 经度
    params[@"latitude"] = @([LJTools getAppDelegate].latitude); // 纬度
//    params[@"longitude"] = @(113.6401); // 经度
//    params[@"latitude"] = @(34.72468); // 纬度
    params[@"city"] = self.city;

    WeakSelf
    [NetworkingTool getWithUrl:kRWHallOrderURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            if (isRefresh) {
                [self.dataArray removeAllObjects];
            }
            NSDictionary *dataDic = responseObject[@"data"];
            NSArray *dataArray = dataDic[@"records"];
            for (NSDictionary *obj in dataArray) {
                GrabOrderModel *model = [[GrabOrderModel alloc] initWithDictionary:obj];
                [self.dataArray addObject:model];
            }
        }
        else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [self addBlankOnView:self.tableView];
        self.noDataView.hidden = weakSelf.dataArray.count != 0;
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [self.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}

- (void)requestForUserInfo
{
    [NetworkingTool getWithUrl:kGetUserInfoURL params:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            User *user = [User mj_objectWithKeyValues:responseObject[@"data"]];
            [User saveUser:user];
            if (user.authMasterStatus.integerValue==2) {
                [self refresh];
                self.certificationView.hidden = YES;
            }else {
                self.certificationView.hidden = NO;
            }
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:NO];
}


#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GrabOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GrabOrderCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"GrabOrderCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    GrabOrderModel *model = self.dataArray[indexPath.row];
    cell.model = model;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    GrabOrderModel *model = self.dataArray[indexPath.row];
    GrabOrderDetailController *vc = [[GrabOrderDetailController alloc] init];
    vc.orderId = model.orderId;
    [self.navigationController pushViewController:vc animated:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 10);
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (void)locationUpdataSuccess
{
    NSString *city = [LJTools getAppDelegate].city;
    if (city.length) {
        self.cityLabel.text = city;
        self.city = city;
        [self refresh];
    }else {
        self.cityLabel.text = @"请选择城市";
        self.city = @"";
    }
}

- (IBAction)certificationButtonAction:(UIButton *)sender {
    PersonCertificationController *vc = [[PersonCertificationController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)cityButtonAction:(UIButton *)sender {
    CityViewController *vc = [[CityViewController alloc] init];
    WeakSelf
    vc.cityPickerBlock = ^(SDCityModel * _Nonnull cityModel) {
        weakSelf.city = cityModel.name;
        weakSelf.cityLabel.text = cityModel.name;
        [weakSelf refresh];
    };
    [self.navigationController pushViewController:vc animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
