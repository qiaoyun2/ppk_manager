//
//  GrabOrderCell.h
//  PPKMaster
//
//  Created by null on 2022/4/14.
//

#import <UIKit/UIKit.h>
#import "GrabOrderModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface GrabOrderCell : UITableViewCell

@property (nonatomic, strong) GrabOrderModel *model;

@end

NS_ASSUME_NONNULL_END
