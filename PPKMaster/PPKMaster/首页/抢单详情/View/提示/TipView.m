//
//  TipView.m
//  PPKMaster
//
//  Created by null on 2022/4/15.
//

#import "TipView.h"

@interface TipView ()

@property (weak, nonatomic) IBOutlet UIView *contentView;


@end

@implementation TipView

- (void)awakeFromNib
{
    [super awakeFromNib];
}

//展示出现
-(void)showPopView{
    /*从中间出现的popview*/
    self.hidden = NO;
    CGAffineTransform transform = CGAffineTransformScale(CGAffineTransformIdentity,1.0,1.0);
    self.contentView.transform = CGAffineTransformScale(CGAffineTransformIdentity,0.2,0.2);
    self.contentView.alpha = 0;
    [UIView animateWithDuration:0.3 delay:0.1 usingSpringWithDamping:0.5 initialSpringVelocity:10 options:UIViewAnimationOptionCurveLinear animations:^{
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.4f];
        self.contentView.transform = transform;
        self.contentView.alpha = 1;
    } completion:^(BOOL finished) {
        
    }];
}

//隐藏出现
-(void)dismissPopView{
    [UIView animateWithDuration:0.3 animations:^{
        self.contentView.transform=CGAffineTransformMakeScale(0.02, 0.02);
    } completion:^(BOOL finished) {
        self.hidden = YES;
        [self removeFromSuperview];
        //        [UIView animateWithDuration:0.08 animations:^{
        //            self.LucyPopView.transform=CGAffineTransformMakeScale(0.25, 0.25);
        //        } completion:^(BOOL finished) {
        //            [self removeFromSuperview];
        //        }];
    }];
}


- (IBAction)confirmButtonAction:(UIButton *)sender {
    [self dismissPopView];
    if (self.onConfirmButtonClick) {
        self.onConfirmButtonClick();
    }
}

- (IBAction)cancelButtonAction:(UIButton *)sender {
    
}

@end
