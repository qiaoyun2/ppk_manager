//
//  TipView.h
//  PPKMaster
//
//  Created by null on 2022/4/15.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TipView : UIView
@property (weak, nonatomic) IBOutlet UILabel *tipLabel;

@property (nonatomic, copy) void(^onConfirmButtonClick)(void);

//展示出现
-(void)showPopView;
//隐藏出现
-(void)dismissPopView;


@end

NS_ASSUME_NONNULL_END
