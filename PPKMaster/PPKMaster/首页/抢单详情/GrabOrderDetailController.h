//
//  GrabOrderDetailController.h
//  PPKMaster
//
//  Created by null on 2022/4/14.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface GrabOrderDetailController : BaseViewController

@property (nonatomic, strong) NSString *orderId;

@end

NS_ASSUME_NONNULL_END
