//
//  GrabOrderDetailController.m
//  PPKMaster
//
//  Created by null on 2022/4/14.
//

#import "GrabOrderDetailController.h"
#import "FoundListImage9TypographyView.h"
#import "TipView.h"
#import "OpenVipView.h"
#import "RWOrderDetailModel.h"
#import "LMMapViewController.h"

@interface GrabOrderDetailController ()
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet FoundListImage9TypographyView *imgsView;
@property (weak, nonatomic) IBOutlet UILabel *orderNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UILabel *numLabel;
@property (weak, nonatomic) IBOutlet UILabel *reserveTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *createTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *remarkLabel;

@property (nonatomic, strong) OpenVipView *vipView;
@property (nonatomic, strong) RWOrderDetailModel *detailModel;

@end

@implementation GrabOrderDetailController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"待接订单";
    self.view.backgroundColor = UIColorFromRGB(0xF5F6F9);
    [self requestForOrderDetail];
}

#pragma mark - Network
- (void)requestForOrderDetail
{
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
    params[@"orderId"] = self.orderId;
    params[@"longitude"] = @([LJTools getAppDelegate].longitude); // 经度
    params[@"latitude"] = @([LJTools getAppDelegate].latitude); // 纬度
//    params[@"longitude"] = @(113.6401); // 经度
//    params[@"latitude"] = @(34.72468); // 纬度
    [NetworkingTool getWithUrl:kRWHallOrderDetailURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            NSDictionary *dataDic = responseObject[@"data"];
            self.detailModel = [[RWOrderDetailModel alloc] initWithDictionary:dataDic];
            [self updateSubViews];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

- (void)requestForTakeOrder
{
    [NetworkingTool postWithUrl:kRWTakeOrderURL params:@{@"orderId":self.orderId} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:@"接单成功，请去订单中查看" delay:1.5];
            [self.navigationController popViewControllerAnimated:YES];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}


#pragma mark - Function
- (void)showVipView
{
    if (!self.vipView) {
        OpenVipView *view = [[[NSBundle mainBundle] loadNibNamed:@"OpenVipView" owner:nil options:nil] firstObject];
        view.frame = [UIScreen mainScreen].bounds;
        [[UIApplication sharedApplication].keyWindow addSubview:view];
        self.vipView = view;
    }
    [self.vipView show];
}

- (void)updateSubViews
{
    self.addressLabel.text = [NSString stringWithFormat:@"%@%@%@%@",_detailModel.province,_detailModel.city,_detailModel.area,_detailModel.street];
    self.nameLabel.text = [NSString stringWithFormat:@"%@ %@",_detailModel.receiver,_detailModel.telphone];
    if (_detailModel.video.length>0) {
        self.imgsView.videoPath = _detailModel.video;
        self.imgsView.imageArray= @[_detailModel.videoPicture];
    }
    else {
        if (_detailModel.picture.length>0) {
            NSArray *images = [_detailModel.picture componentsSeparatedByString:@","];
            self.imgsView.imageArray= images;
        }
    }
    
    self.orderNumLabel.text = _detailModel.orderId;
    self.typeLabel.text = [NSString stringWithFormat:@"%@ %@",_detailModel.firstClassify,_detailModel.secondClassify];
    self.numLabel.text = [NSString stringWithFormat:@"%@件%@吨",_detailModel.goodsNum,_detailModel.goodsWeight];
    self.reserveTimeLabel.text = _detailModel.reserveTime;
    self.createTimeLabel.text = _detailModel.createTime;
    self.remarkLabel.text = _detailModel.remark;
}

#pragma mark - XibFunction
- (IBAction)addressViewTap:(id)sender {
    LMMapViewController *vc = [[LMMapViewController alloc] init];
    vc.coordinate = CLLocationCoordinate2DMake([_detailModel.latitude floatValue], [_detailModel.longitude floatValue]);
    vc.city = _detailModel.city;
    vc.district = _detailModel.area;
    vc.address = _detailModel.street;
//        vc.distance = _detailModel.di;
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)takeOrderButtonAction:(id)sender {
    if ([[User getUser].isMasterVip integerValue]==1) {
        [self requestForTakeOrder];
    }else {
        TipView *view = [[[NSBundle mainBundle] loadNibNamed:@"TipView" owner:nil options:nil] firstObject];
        view.frame = [UIScreen mainScreen].bounds;
        [[UIApplication sharedApplication].keyWindow addSubview:view];
        [view showPopView];
        WeakSelf
        [view setOnConfirmButtonClick:^{
            [weakSelf showVipView];
        }];
    }
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
