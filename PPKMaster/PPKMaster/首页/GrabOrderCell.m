//
//  GrabOrderCell.m
//  PPKMaster
//
//  Created by null on 2022/4/14.
//

#import "GrabOrderCell.h"

@interface GrabOrderCell ()

@property (weak, nonatomic) IBOutlet UILabel *statusLabel; // 状态
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;  // 一级分类 二级分类
@property (weak, nonatomic) IBOutlet UILabel *reserveTimeLabel; // 预约时间
@property (weak, nonatomic) IBOutlet UILabel *addressLabel; // 团购地点
@property (weak, nonatomic) IBOutlet UILabel *createTimeLabel; // 发布时间

@property (weak, nonatomic) IBOutlet UIView *shadowView;

@end

@implementation GrabOrderCell


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.shadowView.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.05].CGColor;
    self.shadowView.layer.shadowOffset = CGSizeMake(0,1.5);
    self.shadowView.layer.shadowRadius = 6;
    self.shadowView.layer.shadowOpacity = 1;
    self.shadowView.layer.cornerRadius = 8;
//    self.bossInfoView.layer.borderColor = UIColorFromRGB(0xBFBFBF).CGColor;
//    self.bossInfoView.layer.borderWidth = 0.8;
}

- (void)setModel:(GrabOrderModel *)model
{
    _model = model;
    self.typeLabel.text = [NSString stringWithFormat:@"%@ %@",model.firstClassify,model.secondClassify];
    self.reserveTimeLabel.text = model.reserveTime;
    self.addressLabel.text = [NSString stringWithFormat:@"%@%@%@%@",model.province,model.city,model.area,model.street];
    self.createTimeLabel.text = model.createTime;
    self.distanceLabel.text = [NSString stringWithFormat:@"%@km",model.distance];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
