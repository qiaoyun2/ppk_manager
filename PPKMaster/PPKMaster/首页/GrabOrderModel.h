//
//  GrabOrderModel.h
//  PPKMaster
//
//  Created by null on 2022/4/22.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface GrabOrderModel : BaseModel

@property (nonatomic, strong) NSString *area;
@property (nonatomic, strong) NSString *cancelTime;
@property (nonatomic, strong) NSString *city; //
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *createTime; // 下单时间
@property (nonatomic, strong) NSString *distance; // 距离
@property (nonatomic, strong) NSString *firstClassify; // 一级分类
@property (nonatomic, strong) NSString *latitude;
@property (nonatomic, strong) NSString *longitude;
@property (nonatomic, strong) NSString *orderId; // 订单号
@property (nonatomic, strong) NSString *province;
@property (nonatomic, strong) NSString *receiveTime; // 接单时间
@property (nonatomic, strong) NSString *reserveTime; // 预约时间
@property (nonatomic, strong) NSString *secondClassify; // 二级分类
@property (nonatomic, strong) NSString *status; //
@property (nonatomic, strong) NSString *street;

@end

NS_ASSUME_NONNULL_END
