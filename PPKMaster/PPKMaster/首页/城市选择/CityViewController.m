//
//  CityViewController.m
//  wonderfulgoods
//
//  Created by null on 2020/1/9.
//  Copyright © 2020 null. All rights reserved.
//

#import "CityViewController.h"
#import "SDCityPickerTableView.h"
#import "SDCityModel.h"

#import "SDCityInitial.h"

#define historyCityFilepath [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"historyCity.data"]

@interface CityViewController ()<SDBaseTableViewDelegate,UITextFieldDelegate>
@property (nonatomic,strong)SDCityPickerTableView *cityTableView;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIView *cityVIew;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top;
//判断是否是搜索状态
@property (nonatomic, assign)BOOL isSearch;
/**
 热门
 */
@property (nonatomic,strong)NSMutableArray *hotArr;
/**
 历史
 */
@property (nonatomic,strong)NSMutableArray *historyArr;

@property (nonatomic,strong)NSMutableArray *historySelectArr;
@property (nonatomic, strong) SDCityModel *localCellItem; // 当前定位的城市

@end



@implementation CityViewController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.isSearch = NO;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    if (@available(iOS 13.0, *)) {
        return UIStatusBarStyleDarkContent;
    } else {
        return UIStatusBarStyleDefault;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (![LJTools getAppDelegate].city) {
        [[LJTools getAppDelegate].locationManager startUpdatingLocation];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationUpdataSuccess) name:LocationChangeSuccess object:nil];
    NSString *city = [LJTools getAppDelegate].city;
    if ([city length] == 0) {
        city = @" 北京市";
    }
    [self.top setConstant:StatusHight];
    [self.cityLabel setText:[NSString stringWithFormat:@"当前地点：%@",city]];
    [self setupTableView];
    [self addNotification];
    
    // 点击城市
    WeakSelf;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cityLabelTap:)];
    [self.cityLabel addGestureRecognizer:tap];
//    [self.cityLabel mdf_whenSingleTapped:^(UIGestureRecognizer *gestureRecognizer) {

//    }];
}

- (void)cityLabelTap:(UITapGestureRecognizer *)tap
{
    if (self.localCellItem) {
        if (self.cityPickerBlock){
            self.cityPickerBlock(self.localCellItem);
        }
    } else {
        SDCityModel *tempModel = [[SDCityModel alloc] init];
        tempModel.name = [LJTools getAppDelegate].city;
        self.localCellItem = tempModel;
        if (self.cityPickerBlock){
            self.cityPickerBlock(self.localCellItem);
        }
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)locationUpdataSuccess{
    NSString *city = [LJTools getAppDelegate].city;
    if ([city length] == 0) {
        city = @" ";
    }
    [self.top setConstant:StatusHight];
    [self.cityLabel setText:[NSString stringWithFormat:@"当前定位城市：%@",city]];
    SDCityModel *tempModel = [[SDCityModel alloc] init];
    tempModel.name = city;
    self.localCellItem = tempModel;
}

- (void)setupTableView{
    [self.view addSubview:self.cityTableView];
    self.cityTableView.sd_delegate =self;
    self.cityTableView.dataArray = self.dataArr;
    [self.cityTableView reloadData];
}

#pragma mark - SDCityPickerTableViewDelegate
- (void)tableView:(id)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    SDCityInitial *cityInitial = self.dataArr[indexPath.section];
    SDCityModel *city = cityInitial.cityArrs[indexPath.row];
    [self.historyArr insertObject:city atIndex:0];
    [self setSelectCityModel:city];
    DLog(@"cityName:%@",city.name);
    if (self.cityPickerBlock){
        self.cityPickerBlock(city);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)addNotification
{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(cityDidSelected:) name:SDCityDidSelectedNotification object:nil];
}

- (void)cityDidSelected:(NSNotification *)notif{
    NSDictionary *userInfo = notif.userInfo;
    SDCityModel *city = userInfo[SDCityDidSelectKey];
    DLog(@"cityName:%@",city.name);
    
    if (self.cityPickerBlock){
        self.cityPickerBlock(city);
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField endEditing:YES];
    return YES;
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

-(SDCityPickerTableView *)cityTableView{
    if (!_cityTableView){

        _cityTableView = [[SDCityPickerTableView alloc] initWithFrame:CGRectMake(0,88+StatusHight, SCREEN_WIDTH, SCREEN_HEIGHT - 88 - StatusHight) style:UITableViewStylePlain];
        _cityTableView.sd_delegate =self;
        [_cityTableView setTableFooterView:[UIView new]];
    }
    return _cityTableView;
}
- (IBAction)backBtnClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)searchBtnClick:(id)sender {

    [self.view endEditing:YES];
}

- (IBAction)textValueChange:(UITextField *)sender {
    [self.dataArr removeAllObjects];
    NSString *path =[[NSBundle mainBundle]pathForResource:@"City" ofType:@"plist"];
    NSArray *arr =[NSArray arrayWithContentsOfFile:path];
    NSMutableArray *cityModels = [NSMutableArray array];
    //获取全部城市cityModel
    for (NSDictionary *dic in arr){
        for (NSDictionary *dict in dic[@"children"]){
            SDCityModel *cityModel = [SDCityModel cityWithDict:dict];
            if (sender.text.length == 0) {
               [cityModels addObject:cityModel];
            }else{
                if ([cityModel.name containsString:self.searchTextField.text] || [self.searchTextField.text containsString:cityModel.name]) {
                    [cityModels addObject:cityModel];
                }
            }
        }
    }
    //获取首字母
    NSMutableArray *indexArr =
    [[cityModels valueForKeyPath:@"firstLetter"] valueForKeyPath:@"@distinctUnionOfObjects.self"];
    //遍历数组
    for (NSString *indexStr in indexArr) {
        
        SDCityInitial *cityInitial =[[SDCityInitial alloc]init];
        cityInitial.initial = indexStr;
        NSMutableArray *cityArrs =[NSMutableArray array];
        for ( SDCityModel *cityModel in cityModels) {
            if ([indexStr isEqualToString:cityModel.firstLetter]) {
                [cityArrs addObject:cityModel];
            }
        }
        cityInitial.cityArrs = cityArrs;
        [_dataArr addObject:cityInitial];
    }
    [_dataArr insertObjects:self.hotArr atIndexes:[NSIndexSet indexSetWithIndex:0]];
    [_dataArr insertObjects:self.historyArr atIndexes:[NSIndexSet indexSetWithIndex:0]];
    [self.cityTableView reloadData];
}

- (IBAction)locationButtonAction:(id)sender {
}


-(void)setSelectCityModel:(SDCityModel *)city{
    
    [self.historyArr removeAllObjects];
    
    SDCityInitial *cityInitial = [[SDCityInitial alloc]init];
    cityInitial.initial = @"定位/最近访问";
    
    [self historySelectArr];
//        [_historySelectArr removeObject:city];
//        [_historySelectArr insertObject:city atIndex:0];
//
    NSMutableArray *emptyArr =[NSMutableArray arrayWithArray:_historySelectArr];
    
//        for (SDCityModel *hiscity in emptyArr) {
//            if ([hiscity.name isEqualToString:city.name]) {
//                [_historySelectArr removeObject:city];
//            }
//        }
//
    [emptyArr enumerateObjectsUsingBlock:^(SDCityModel  *_Nonnull hiscity, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if ([hiscity.name isEqualToString:city.name]) {
            [_historySelectArr removeObjectAtIndex:idx];
            *stop =YES;
        }
        
    }];
    
    
    [_historySelectArr insertObject:city atIndex:0];
    
    if (_historySelectArr.count>8){
        [_historySelectArr removeLastObject];
    }
    
    [NSKeyedArchiver archiveRootObject:_historySelectArr toFile:historyCityFilepath];
    
    cityInitial.cityArrs = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithFile:historyCityFilepath]];
    
    [self.historyArr addObject:cityInitial];
    [self.dataArr replaceObjectsAtIndexes:[NSIndexSet indexSetWithIndex:1] withObjects:self.historyArr];
    [self.cityTableView reloadData];
}
/**
 历史
 
 */
-(NSMutableArray *)historyArr{
    if (!_historyArr){
        _historyArr = [NSMutableArray array];
        SDCityInitial *cityInitial =[[SDCityInitial alloc]init];
        cityInitial.initial = @"#";
        cityInitial.cityArrs = self.historySelectArr;
        [_historyArr addObject:cityInitial];
    }
    return _historyArr;
}
-(NSMutableArray *)historySelectArr{
    if (!_historySelectArr){
        _historySelectArr = [NSKeyedUnarchiver unarchiveObjectWithFile:historyCityFilepath];
        if (!_historySelectArr){
            _historySelectArr =[NSMutableArray array];
        }
    }
    return _historySelectArr;
}
/**
 热门
 
 */
-(NSMutableArray *)hotArr{
    if(!_hotArr){
        _hotArr = [NSMutableArray array];
        SDCityInitial *cityInitial =[[SDCityInitial alloc]init];
        cityInitial.initial = @"热";
        
        NSArray *hotCityArr =@[@{@"id":@"1",@"name":@"北京",@"pid":@"11"},
                               @{@"id":@"2",@"name":@"上海",@"pid":@"11"},
                               @{@"id":@"3",@"name":@"广州",@"pid":@"11"},
                               @{@"id":@"4",@"name":@"深圳",@"pid":@"11"},
                               @{@"id":@"4",@"name":@"成都",@"pid":@"11"},
                               @{@"id":@"4",@"name":@"杭州",@"pid":@"11"},
        ];
        NSMutableArray *hotarrs =[NSMutableArray array];
        
        for (NSDictionary *dic in hotCityArr){
            SDCityModel *city = [SDCityModel cityWithDict:dic];
            [hotarrs addObject:city];
        }
        cityInitial.cityArrs = hotarrs;
        [_hotArr addObject:cityInitial];
    }
    return _hotArr;
    
}

-(NSMutableArray *)dataArr{
    if (!_dataArr){
        _dataArr =[NSMutableArray array];
        NSString *path =[[NSBundle mainBundle]pathForResource:@"City" ofType:@"plist"];
        NSArray *arr =[NSArray arrayWithContentsOfFile:path];
        NSMutableArray *cityModels = [NSMutableArray array];
        //获取全部城市cityModel
        for (NSDictionary *dic in arr){
            for (NSDictionary *dict in dic[@"children"]){
                SDCityModel *cityModel = [SDCityModel cityWithDict:dict];
                [cityModels addObject:cityModel];
            }
        }
        //获取首字母
        NSMutableArray *indexArr =
        [[cityModels valueForKeyPath:@"firstLetter"] valueForKeyPath:@"@distinctUnionOfObjects.self"];
        //遍历数组
        for (NSString *indexStr in indexArr) {
            
            SDCityInitial *cityInitial =[[SDCityInitial alloc]init];
            cityInitial.initial = indexStr;
            NSMutableArray *cityArrs =[NSMutableArray array];
            for ( SDCityModel *cityModel in cityModels) {
                if ([indexStr isEqualToString:cityModel.firstLetter]) {
                    [cityArrs addObject:cityModel];
                }
            }
            cityInitial.cityArrs = cityArrs;
            [_dataArr addObject:cityInitial];
        }
        
        [_dataArr insertObjects:self.hotArr atIndexes:[NSIndexSet indexSetWithIndex:0]];
        [_dataArr insertObjects:self.historyArr atIndexes:[NSIndexSet indexSetWithIndex:0]];
        [self.cityTableView reloadData];
    }
    return _dataArr;
}




@end
