//
//  SDCityPickerTableView.m
//  miaohu
//
//  Created by slowdony on 2017/8/28.
//  Copyright © 2017年 SlowDony. All rights reserved.
//

#import "SDCityPickerTableView.h"
#import "SDCityModel.h"
#import "SDCityInitial.h"

#import "SDCityPickerTableViewCell.h"
#import "SDCityPickerNormalCell.h"

@implementation SDCityPickerTableView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
    self =[super initWithFrame:frame style:style];
    if (self) {
        self.dataSource=self;
        self.delegate=self;
        self.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.sectionIndexColor = MainColor;
        self.sectionIndexColor = RGB(51, 51, 51);
        [self setupUI];
    }
    return self;
}


-(void)setupUI{
    
}

#pragma mark ----------UITabelViewDataSource----------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return  self.dataArray.count ;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0 || section == 1 ){
        return 1;
    }else {
        SDCityInitial  *cityInitial = self.dataArray[section];
        return  cityInitial.cityArrs.count;
    }
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"SDCityPickerNormalCell";
    SDCityPickerNormalCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SDCityPickerNormalCell" owner:nil options:nil] firstObject];
    }
    
    SDCityPickerTableViewCell *cityCell = [SDCityPickerTableViewCell cellWithTableView:self];
    //配置数据
    //    省
    SDCityInitial *cityInitial = self.dataArray[indexPath.section];
    if (cityInitial.cityArrs.count ==0){
        return cell;
    }else {
        SDCityModel *city = cityInitial.cityArrs[indexPath.row];
        if (indexPath.section == 0 ||indexPath.section ==1 ){
            cityCell.dataArr = [NSMutableArray arrayWithArray:cityInitial.cityArrs];
            cityCell.backgroundColor = [UIColor redColor];
            return cityCell;
        }else {
            cell.cityLabel.text = city.name;
            return cell;
        }
    }
}

#pragma mark ----------UITabelViewDelegate----------

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    if (indexPath.section<2) {
//        return;
//    }
    if([self.sd_delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)]){
        [self.sd_delegate tableView:self didSelectRowAtIndexPath:indexPath];
    }
}
- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index{
    
    for (SDCityInitial *cityInitial in self.dataArray) {
        if ([cityInitial.initial isEqualToString:title]) {
            return [self.dataArray indexOfObject:cityInitial];
        }
    }
    return index;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 ||indexPath.section ==1 ){
        SDCityInitial *cityInitial = self.dataArray[indexPath.section];
        CGFloat cellHeight = 0;
        if (cityInitial.cityArrs.count ==0){
            return cellHeight ;
        }
        else {
            if (0 == (cityInitial.cityArrs.count % 4)) {
                cellHeight = cityInitial.cityArrs.count / 4 * (32+10)+2;
            }else {
                cellHeight = (cityInitial.cityArrs.count / 4 + 1) * (32+10)+2;
            }
//            if (cellHeight > 300) {
//                cellHeight = 300;
//            }
            return cellHeight ;
        }
  
    }else {
        return 45;
    }
  
}



/**
 索引值
 
 */
-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    //数组调用valueForKeyPath 返回一个 SDCityInitial模型里的initial属性的值,放在一个数组中返回
    NSMutableArray *indexArrs =[NSMutableArray array];
    for ( SDCityInitial *cityInitial in self.dataArray){
        if (cityInitial.cityArrs.count>0)
        {
            [indexArrs addObject:cityInitial.initial];
        }
    }
    return  indexArrs;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    SDCityInitial *cityInitial = self.dataArray[section];
    return (cityInitial.cityArrs.count)==0 ? 0:32;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headview = [[UIView alloc] init];
    headview.frame = CGRectMake(0, 0, mDeviceWidth, 32);
    headview.backgroundColor = UIColorFromRGB(0xF6F7F9);
    //标题
    UILabel *headlabel = [[UILabel alloc] init];
    headlabel.frame = CGRectMake(16, 0, mDeviceWidth, 32);
    headlabel.textColor = UIColorFromRGB(0x666666);
    SDCityInitial *cityInitial = self.dataArray[section];
    headlabel.text = [NSString stringWithFormat:@"%@",cityInitial.initial];
    if ([cityInitial.initial isEqualToString:@"#"]) {
        [headlabel setText:@"历史浏览城市"];
        headview.backgroundColor = [UIColor whiteColor];
        headlabel.textColor = UIColorFromRGB(0x999999);
    }
    if ([cityInitial.initial isEqualToString:@"热"]) {
        [headlabel setText:@"热门城市"];
        headview.backgroundColor = [UIColor whiteColor];
        headlabel.textColor = UIColorFromRGB(0x999999);
    }
    headlabel.textAlignment = NSTextAlignmentLeft;
    headlabel.font = [UIFont systemFontOfSize:12];
    headlabel.numberOfLines = 1;
    [headview addSubview:headlabel];
    return headview;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

@end
