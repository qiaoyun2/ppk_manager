//
//  SDCityPickerNormalCell.h
//  PPKMaster
//
//  Created by null on 2022/4/26.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SDCityPickerNormalCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;

@end

NS_ASSUME_NONNULL_END
