//
//  PasswordLoginController.m
//  PPK
//
//  Created by null on 2022/3/3.
//

#import "PasswordLoginController.h"
#import "ForgetPasswordViewController.h"
#import "RWTabbarController.h"

@interface PasswordLoginController ()
@property (weak, nonatomic) IBOutlet UITextField *mobileField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UIButton *secureButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoImageViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeight;

@end

@implementation PasswordLoginController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.logoImageViewTop.constant = 40+StatusHight;
    self.contentViewHeight.constant = SCREEN_HEIGHT > 667 ? SCREEN_HEIGHT : 667;
}

- (IBAction)loginButtonAction:(id)sender {
    [self.view endEditing:YES];
    if (self.mobileField.text.length == 0) {
        [LJTools showNOHud:@"请输入手机号" delay:1];
        return;
    }
    if (![self.mobileField.text isTelephone]) {
        [LJTools showNOHud:@"请输入正确的手机号" delay:1];
        return;
    }
    if ([self.passwordField.text isBlankString]) {
        [LJTools showNOHud:@"请输入密码" delay:1];
        return;
    }
//    if ([self.passwordField.text isPassword]) {
//        [LJTools showNOHud:@"请输入密码" delay:1];
//        return;
//    }
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"mobile"] = self.mobileField.text;
    parameters[@"password"] = self.passwordField.text;
    [NetworkingTool postWithUrl:kMobileLoginURL params:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            NSDictionary *userDic = responseObject[@"data"][@"user"];
            User *user = [User mj_objectWithKeyValues:userDic];
            [User saveUser:user];
            [UIApplication sharedApplication].keyWindow.rootViewController = [[TabBarController alloc] init];
        } else {
            
        }
        [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];

}

- (IBAction)secureButtonAction:(UIButton *)sender {
    sender.selected = !sender.selected;
}

- (IBAction)codeLoginButtonAction:(id)sender {
    [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)forgetButtonAction:(id)sender {
    ForgetPasswordViewController *vc = [[ForgetPasswordViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)thirdButtonAction:(UIButton *)sender {
    
}


@end
