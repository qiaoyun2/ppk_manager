//
//  FeedbackVC.m
//  ZZR
//
//  Created by null on 2021/2/20.
//

#import "FeedbackVC.h"
#import "PhotosView.h"
#import "FeedBackListVC.h"
#import <MobileCoreServices/UTCoreTypes.h>
#import <TZImagePickerController/TZImagePickerController.h>
#import "UITextView+ZWPlaceHolder.h"
#import "UploadManager.h"
#import <HXPhotoPicker/HXPhotoPicker.h>
#import "HXAssetManager.h"



@interface FeedbackVC ()<UITextViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,TZImagePickerControllerDelegate,HXPhotoViewDelegate>
{

}
@property (weak, nonatomic) IBOutlet UIView *tagView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tagViewHeight;
@property (weak, nonatomic) IBOutlet UIButton *btn;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UITextField *contentLabel;
@property (weak, nonatomic) IBOutlet UITextField *contactTf;
@property (weak, nonatomic) IBOutlet HXPhotoView *photoView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *photoViewHeight;

@property(nonatomic,strong) NSArray *reasons;
@property(nonatomic,strong) NSMutableArray *btns;
@property(nonatomic,strong) UIButton *selectBtn;

@property (nonatomic, strong) NSMutableArray<UIImage *> *photos;
@property (nonatomic, strong) NSMutableArray *imagePathArray;
@property (strong, nonatomic) HXPhotoManager *manager;


@end

@implementation FeedbackVC
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"我要反馈";
    _btns = [NSMutableArray new];
    _textView.placeholder = @"问题描述的越详细，有助于我们更快的解决问题";
    _textView.delegate = self;

    self.photos = [NSMutableArray array];
    self.imagePathArray = [NSMutableArray array];
    self.photoView.spacing = 10.f;
    self.photoView.delegate = self;
    self.photoView.deleteCellShowAlert = YES;
    self.photoView.outerCamera = YES;
    self.photoView.previewShowDeleteButton = YES;
    self.photoView.addImageName = @"组 51192";
    self.photoView.lineCount = 4;
    self.photoView.manager = self.manager;
    
    [self setNavigationRightBarButtonWithTitle:@"反馈记录" color:MainColor];

    [self getInfo];
}

#pragma mark – UI
-(void)fillWithTagInfo:(NSArray*)tags{
    [_tagView removeAllSubviews];
    if (tags.count<1) {
        _tagViewHeight.constant = 0;
    }else{
        CGFloat item_w = (SCREEN_WIDTH-32-40)/3;
        CGFloat maxHeight = 0;
        for (int i = 0; i<tags.count; i++) {
            NSDictionary * dict = tags[i];
            int hang = i/3;
            int row = i%3;
            UIButton * btn = [[UIButton alloc]initWithFrame:CGRectMake(16+row*(item_w+20), hang*(16+36), item_w, 36)];
            btn.titleLabel.font = [UIFont systemFontOfSize:14];
            [btn setTitle:dict[@"content"] forState:0];
            [btn setTitleColor:UIColorFromRGB(0x999999) forState:0];
            btn.layer.cornerRadius = 4;
            btn.layer.masksToBounds = YES;
            btn.layer.borderColor = UIColorFromRGB(0xBFBFBF).CGColor;
            btn.layer.borderWidth = 0.5f;
            [_btns addObject:btn];
            btn.tag = [dict[@"id"] intValue];
            [btn addTarget:self action:@selector(reasonClick:) forControlEvents:UIControlEventTouchUpInside];
            [_tagView addSubview:btn];
            if (i == tags.count-1) {
                maxHeight = CGRectGetMaxY(btn.frame);
            }
        }
        _tagViewHeight.constant = maxHeight+16;
    }
}

#pragma mark – Network
- (void)getInfo{
    [NetworkingTool getWithUrl:kFeedBackTypeListURL params:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"]intValue]==SUCCESS) {
            NSArray *dataList = responseObject[@"data"];
            [self fillWithTagInfo:dataList];
        }else{
            [LJTools showNOHud:responseObject[@"msg"] delay:1];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

/** 意见反馈--提交反馈 **/
- (void)setAddFeedback {

//    type【投诉建议类型名称。类型：varchar(256)】
    //body【内容。类型：varchar(1000)】
    //thumb【缩略图。类型：varchar(255)】
    //contact【联系方式。类型：varchar(255)】
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:@(_selectBtn.tag) forKey:@"typeIds"];
    [params setValue:self.textView.text forKey:@"content"];
    [params setValue:[self.imagePathArray componentsJoinedByString:@","] forKey:@"imgArray"];
    [params setValue:self.contactTf.text forKey:@"contact"];
    WeakSelf;
    [NetworkingTool postWithUrl:kAddFeedBackURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            [LJTools showText:LocalizedString(@"反馈成功") delay:1];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }else{
            [LJTools showNOHud:responseObject[@"msg"] delay:1];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}

- (void)requestForUploadImages
{
    WeakSelf
    [UploadManager uploadImageArray:self.photos block:^(NSString * _Nonnull ids, NSString * _Nonnull imageUrl) {
        NSArray *array = [imageUrl componentsSeparatedByString:@","];
        [weakSelf.imagePathArray addObjectsFromArray:array];
        [weakSelf setAddFeedback];
    }];
}


#pragma mark – Delegate
-(void)textViewDidChangeSelection:(UITextView *)textView{
    if (textView.text.length>200) {
        textView.text = [textView.text substringToIndex:200];
    }
}

#pragma mark - HXPhotoViewDelegate
- (void)photoView:(HXPhotoView *)photoView changeComplete:(NSArray<HXPhotoModel *> *)allList photos:(NSArray<HXPhotoModel *> *)photos videos:(NSArray<HXPhotoModel *> *)videos original:(BOOL)isOriginal {
    [self.photos removeAllObjects];
    for (HXPhotoModel *model in photos) {
        [self.photos addObject:[HXAssetManager originImageForAsset:model.asset]];
    }
}

// 拿到view变化的高度
- (void)photoView:(HXPhotoView *)photoView updateFrame:(CGRect)frame {
    self.photoViewHeight.constant = frame.size.height;
}


#pragma mark - Function
-(void)contentDidChange:(UITextField*)textField{
    NSLog(@"%@",textField.text);
    CGFloat maxLength = 30;
    NSString *toBeString = textField.text;
    UITextRange *selectedRange = [textField markedTextRange];
    UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
    if (!position || !selectedRange)
    {
        if (toBeString.length > maxLength)
        {
            NSRange rangeIndex = [toBeString rangeOfComposedCharacterSequenceAtIndex:maxLength];
            if (rangeIndex.length == 1)
            {
                textField.text = [toBeString substringToIndex:maxLength];
            }
            else
            {
                NSRange rangeRange = [toBeString rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, maxLength)];
                textField.text = [toBeString substringWithRange:rangeRange];
            }
        }
    }
    
}

- (void)reasonClick:(UIButton*)sender{
    _selectBtn = sender;
    [self changeColor:_selectBtn];
}

- (void)changeColor:(UIButton*)sender{
    for (UIButton * btn  in _btns) {
        if (btn == sender) {
            [btn setTitleColor:[UIColor whiteColor] forState:0];
            btn.backgroundColor = MainColor;
            btn.layer.borderColor = [UIColor clearColor].CGColor;
        }else{
            [btn setTitleColor:UIColorFromRGB(0x999999) forState:0];
            btn.backgroundColor = [UIColor clearColor];
            btn.layer.borderColor = UIColorFromRGB(0xBFBFBF).CGColor;
        }
    }
}

- (void)rightButtonTouchUpInside:(UIBarButtonItem *)sender{    FeedBackListVC *vc = [[FeedBackListVC alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark – XibFunction
- (IBAction)btnClick:(id)sender {
    [self.view endEditing:YES];
    if (!_selectBtn) {
        [LJTools showText:LocalizedString(@"请选择反馈类型") delay:1];
        return;
    }
    if (_textView.text.length<1) {
        [LJTools showText:LocalizedString(@"请填写反馈内容") delay:1];
        return;
    }
    if (self.contactTf.text.length < 1) {
        [LJTools showText:LocalizedString(@"请填写联系方式") delay:1];
        return;
    }
    if ([self.contactTf.text isTelephone] || [self.contactTf.text isEmail] || [self.contactTf.text validateQQ]) {} else {
        [LJTools showNOHud:LocalizedString(@"请输入有效的联系方式") delay:1];
        return;
    }
    if (self.photos.count) {
        [self requestForUploadImages];
    }else {
        [self setAddFeedback];
    }
}

#pragma mark - Lazy Load
- (HXPhotoManager *)manager {
    if (!_manager) {
        _manager = [[HXPhotoManager alloc] initWithType:HXPhotoManagerSelectedTypePhoto];
        _manager.configuration.type = HXConfigurationTypeWXChat;
        _manager.configuration.singleSelected = NO;
        _manager.configuration.lookGifPhoto = NO;
        _manager.configuration.useWxPhotoEdit = YES;
        _manager.configuration.selectTogether=NO;
        _manager.configuration.photoEditConfigur.onlyCliping = YES;
        _manager.configuration.showOriginalBytes = NO;
        _manager.configuration.videoMaxNum=0;
        _manager.configuration.photoMaxNum=9;
        _manager.configuration.requestImageAfterFinishingSelection = YES;
        _manager.configuration.photoEditConfigur.aspectRatio = HXPhotoEditAspectRatioType_None;
    }
    return _manager;
}
@end
