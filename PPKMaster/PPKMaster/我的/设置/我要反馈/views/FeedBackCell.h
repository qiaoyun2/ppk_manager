//
//  FeedBackCell.h
//  ZZR
//
//  Created by null on 2021/7/7.
//

#import <UIKit/UIKit.h>
#import "FeedBackModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface FeedBackCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headImg;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UIView *imgsView;
@property (weak, nonatomic) IBOutlet UILabel *contactLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIView *replyView;
@property (weak, nonatomic) IBOutlet UILabel *replyContentLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgsViewHeight;

@property(nonatomic,strong)FeedBackModel * model;

@end

NS_ASSUME_NONNULL_END
