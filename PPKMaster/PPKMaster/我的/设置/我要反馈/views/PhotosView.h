//
//  PhotosView.h
//  ZZR
//
//  Created by null on 2020/8/27.
//  Copyright © 2020 null. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PhotosView : UIView

@property (nonatomic, copy) void(^click)(NSInteger index);

-(instancetype)initWithFrame:(CGRect)frame withType:(NSInteger)type;//1,横向 2.9宫格

@property(nonatomic,strong)NSArray * images;

@property(nonatomic,assign)NSInteger maxCount;

@property(nonatomic,assign)NSInteger rowCount;

@property(nonatomic,assign)CGSize imgSize;

@property(nonatomic,strong)UIScrollView * scrollView;

@property(nonatomic,copy)NSString * defaultImg;

@property(nonatomic,copy)NSString * delImg;

@end

NS_ASSUME_NONNULL_END
