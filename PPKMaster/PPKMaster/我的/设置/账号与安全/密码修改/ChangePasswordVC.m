//
//  ChangePasswordVC.m
//  ZZR
//
//  Created by null on 2021/1/28.
//

#import "ChangePasswordVC.h"

@interface ChangePasswordVC ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *tf1;
@property (weak, nonatomic) IBOutlet UITextField *tf2;
@property (weak, nonatomic) IBOutlet UIView *oldPassWordView;
@property (weak, nonatomic) IBOutlet UITextField *oldPswTF;

@end

@implementation ChangePasswordVC
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"修改密码";
    if (self.type==1) {
        _oldPassWordView.hidden = YES;
    }else{
        _oldPassWordView.hidden = NO;
    }
    [_tf1 addTarget:self action:@selector(contentDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_tf2 addTarget:self action:@selector(contentDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_oldPswTF addTarget:self action:@selector(contentDidChange:) forControlEvents:UIControlEventEditingChanged];
}
#pragma mark – UI
#pragma mark – Network
#pragma mark – Delegate
#pragma mark - Function
- (void)contentDidChange:(UITextField*)textField{
    NSLog(@"%@",textField.text);
    CGFloat maxLength = 12;
    NSString *toBeString = textField.text;
    UITextRange *selectedRange = [textField markedTextRange];
    UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
    if (!position || !selectedRange)
    {
        if (toBeString.length > maxLength)
        {
            NSRange rangeIndex = [toBeString rangeOfComposedCharacterSequenceAtIndex:maxLength];
            if (rangeIndex.length == 1)
            {
                textField.text = [toBeString substringToIndex:maxLength];
            }
            else
            {
                NSRange rangeRange = [toBeString rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, maxLength)];
                textField.text = [toBeString substringWithRange:rangeRange];
            }
        }
    }
    
}
#pragma mark – XibFunction
- (IBAction)btnClick:(id)sender {
    [self.view endEditing:YES];
    if (self.type == 2) {
        if (_oldPswTF.text.length<1) {
            [LJTools showText:LocalizedString(@"请输入旧密码") delay:1];
            return;
        }
    }
    
    if (_tf1.text.length<1) {
        [LJTools showText:LocalizedString(@"请输入密码") delay:1];
        return;
    }
    
    if (_tf1.text.length<6) {
        [LJTools showText:LocalizedString(@"请输入6~12位密码") delay:1];
        return;
    }
    
    if (_tf1.text.length>12) {
        [LJTools showText:LocalizedString(@"请输入6~12位密码") delay:1];
        return;
    }
    
    if (_tf2.text.length<1) {
        [LJTools showText:LocalizedString(@"请输入密码") delay:1];
        return;
    }

    if (_tf1.text.length<6) {
        [LJTools showText:LocalizedString(@"请输入6~12位密码") delay:1];
        return;
    }
    if (![_tf1.text isEqualToString:_tf2.text]) {
        [LJTools showText:LocalizedString(@"两次密码不一致") delay:1];
        return;
    }
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
    if (self.type == 2) {
        [params setValue:_oldPswTF.text forKey:@"oldPassword"];
        [params setValue:_tf1.text forKey:@"newPassword"];
        [params setValue:_tf1.text forKey:@"confirmPassword"];
        [NetworkingTool postWithUrl:kChangePwdByOldURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
            if ([responseObject[@"code"]intValue]==SUCCESS) {
                [LJTools showText:responseObject[@"msg"] delay:1];
                [self.navigationController jk_popToViewControllerWithClassName:@"SettingController" animated:YES];
            }else{
                [LJTools showNOHud:responseObject[@"msg"] delay:1];
            }
        } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
            
        } IsNeedHub:YES];
    }else{

        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
        [params setValue:self.tf1.text forKey:@"password"];
        [params setValue:@"verifyPhone" forKey:@"event"];
        [params setValue:self.code forKey:@"captcha"];
        [NetworkingTool postWithUrl:kChangePwdByCodeSecondURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
            [LJTools hideHud];
            if ([responseObject[@"code"]intValue]==SUCCESS) {
                [LJTools showNOHud:responseObject[@"msg"] delay:1];
                [self.navigationController jk_popToViewControllerWithClassName:@"SettingController" animated:YES];

            }else{
                [LJTools showNOHud:responseObject[@"msg"] delay:1];
            }
        } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
            [LJTools hideHud];
        } IsNeedHub:YES];
    }
}
#pragma mark – lazy load
@end
