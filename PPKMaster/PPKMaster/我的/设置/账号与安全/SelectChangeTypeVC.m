//
//  SelectChangeTypeVC.m
//  ZZR
//
//  Created by null on 2021/3/8.
//

#import "SelectChangeTypeVC.h"
#import "ChangePasswordVC.h"
#import "VerificationPhoneVC.h"
#import "LogoutViewController.h"
#import "LogoutViewController4.h"
#import "SelectChangeTypeVC1.h"
#import "SelectChangeTypeVC2.h" // 修改支付密码选择



@interface SelectChangeTypeVC ()

@property (weak, nonatomic) IBOutlet UILabel *payPasswordLabel;
@property (weak, nonatomic) IBOutlet UIView *payPasswordView;
@property (nonatomic, assign) BOOL isPayPassword;
@end

@implementation SelectChangeTypeVC
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self requestForIsPayPassword];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xF6F7F9);
    self.navigationItem.title = LocalizedString(@"账号与安全");
    self.payPasswordView.hidden = [LJTools getAppDelegate].isOnline;
}

#pragma mark - UI
#pragma mark - Network
- (void)requestForIsPayPassword
{
    [NetworkingTool getWithUrl:kIsPayPasswordURL params:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            self.isPayPassword = [responseObject[@"data"] integerValue];
            self.payPasswordLabel.text = self.isPayPassword ? @"修改支付密码":@"设置支付密码";
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:NO];
}

#pragma mark - Delegate
#pragma mark - Function
#pragma mark - XibFunction
- (IBAction)btnClick:(UIButton*)sender {
    //1.修改密码 2.修改支付密码 3修改绑定手机号
    if (sender.tag == 1) {
        SelectChangeTypeVC1 *vc = [[SelectChangeTypeVC1 alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if (sender.tag == 2) {
        if (self.isPayPassword) {
            SelectChangeTypeVC2 *vc = [[SelectChangeTypeVC2 alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
        else {
            VerificationPhoneVC *vc = [[VerificationPhoneVC alloc] init];
            vc.type = 2;
            [self.navigationController pushViewController:vc animated:YES];
        }
    }else{
        VerificationPhoneVC *vc = [[VerificationPhoneVC alloc] init];
        vc.type = 1;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

/// 注销
- (IBAction)logoutButtonClick:(UIButton *)sender {
    User *user = [User getUser];
    // 注销审核状态：0未提交；1待审核；2审核通过；3审核不通过
    if ([user.delStatus integerValue] == 1) {
        LogoutViewController4 *vc1 = [[LogoutViewController4 alloc] init];
        vc1.reason = user.delReason;
        [self.navigationController pushViewController:vc1 animated:YES];
    }
    else {
        LogoutViewController *vc = [[LogoutViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }
}


#pragma mark – lazy load
@end
