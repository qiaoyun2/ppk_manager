//
//  ChangePayPasswordVC.m
//  ZZR
//
//  Created by null on 2020/9/11.
//  Copyright © 2020 null. All rights reserved.
//

#import "ChangePayPasswordVC.h" // 修改支付密码

#import "HLCircleCodeView.h"

@interface ChangePayPasswordVC ()

@property (weak, nonatomic) IBOutlet UILabel *oneLabel;
@property (weak, nonatomic) IBOutlet UILabel *twoLabel;
@property (weak, nonatomic) IBOutlet UILabel *threeLabel;
@property (weak, nonatomic) IBOutlet UIProgressView *lineView;


@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@property (weak, nonatomic) IBOutlet UIView *bView;
@property (nonatomic, strong) HLCircleCodeView *circleCodeView;


@end

@implementation ChangePayPasswordVC
-(void)viewWillAppear:(BOOL)animated{

    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [self.circleCodeView becomeFirstResponder];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.circleCodeView resignFirstResponder];

}
- (void)viewDidLoad {
    [super viewDidLoad];
    
//#BFBFBF #FF440A
    
    self.navigationItem.title = @"";
    self.view.backgroundColor = [UIColor whiteColor];
    [self.bView addSubview:self.circleCodeView];
    [self.view layoutIfNeeded];
    if (self.type == 1) {
        self.titleLabel.text = @"修改支付密码";
        self.contentLabel.text = @"请输入支付密码 以验证身份";
        self.lineView.progress = 0.0;
        self.oneLabel.backgroundColor = MainColor;
    }
    else if (self.type == 2) {
        self.titleLabel.text = @"设置支付密码";
        self.contentLabel.text = @"请设置新支付密码";
        self.lineView.progress = 0.5;
        self.oneLabel.backgroundColor = MainColor;
        self.twoLabel.backgroundColor = MainColor;
    }
    else {
        self.titleLabel.text = @"设置支付密码";
        self.contentLabel.text = @"请再次输入新支付密码";
        self.lineView.progress = 1;
        self.oneLabel.backgroundColor = MainColor;
        self.twoLabel.backgroundColor = MainColor;
        self.threeLabel.backgroundColor = MainColor;
    }

}
#pragma mark – UI
#pragma mark – Network
#pragma mark – Delegate
#pragma mark - Function

- (void) intputFinish:(NSString *) password{
    if (self.type == 1) {
        ChangePayPasswordVC *vc = [[ChangePayPasswordVC alloc] init];
        vc.type = 2;
        vc.oldPwd = password;
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if (self.type == 2) {
        ChangePayPasswordVC *vc = [[ChangePayPasswordVC alloc] init];
        vc.type = 3;
        vc.oldPwd = self.oldPwd;
        vc.code = self.code;
        vc.surePwd = password;
        [self.navigationController pushViewController:vc animated:YES];
    }
    else{
        if ([password isEqualToString:self.surePwd]) {
            [self btnClick];
        }else{
            [LJTools showText:@"两次输入不一致" delay:1];
        }
    }

}

#pragma mark – XibFunction

/** 设置支付密码 **/
- (void)btnClick{
    [self.view endEditing:YES];
    NSString *url = @"";
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];

    if (self.oldPwd.length) {
        [params setValue:self.oldPwd forKey:@"oldPassword"];
        [params setValue:self.surePwd forKey:@"newPassword"];
        url = kResetPayPasswordURL;
    }else{
        [params setValue:[User getUser].mobile forKey:@"mobile"];
        [params setValue:@"changePayPwd" forKey:@"event"];
        [params setValue:self.code forKey:@"captcha"];
        [params setValue:self.surePwd forKey:@"payPassword"];
        url = kSetPayPasswordURL;
    }
//    pay_password    String[字符串]    选填            原支付密码
//    new_pay_password    String[字符串]    选填            新支付密码
//    type    Integer[整数]    选填            1原密码重置新密码 2短信重置密码
//    code    String[字符串]    选填            短信验证码
    
    [NetworkingTool postWithUrl:url params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"]intValue]==SUCCESS) {
            [LJTools showNOHud:responseObject[@"msg"] delay:1];
            [self.navigationController jk_popToViewControllerWithClassName:@"SettingController" animated:YES];

        }else{
            [LJTools showNOHud:responseObject[@"msg"] delay:1];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}


#pragma mark – lazy load
#pragma mark - 💓💓 Lazy Loads ------
- (HLCircleCodeView *)circleCodeView {
    if (!_circleCodeView) {
        _circleCodeView = [[HLCircleCodeView alloc] initWithCount:6 margin:-0.5];
        _circleCodeView.frame = CGRectMake(40, 0, SCREEN_WIDTH-80, 50);
        MJWeakSelf;
        _circleCodeView.hl_circleCode = ^(NSString * _Nonnull circleCode) {
            [weakSelf intputFinish:circleCode];
        };
    }
    return _circleCodeView;
}

@end
