//
//  LogoutViewController3.m
//  ZZR
//
//  Created by ZZR on 2021/10/7.
//

#import "LogoutViewController4.h"

@interface LogoutViewController4 ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (weak, nonatomic) IBOutlet UILabel *titleLable;

@property (weak, nonatomic) IBOutlet UILabel *reasonLabel;

@end

@implementation LogoutViewController4

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = UIColorFromRGB(0xFFFFFF);
    self.navigationItem.title = LocalizedString(@"注销账号");
    self.reasonLabel.text = [NSString stringWithFormat:@"%@%@",LocalizedString(@"注销理由："),self.reason];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
