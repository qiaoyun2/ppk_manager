//
//  LogoutViewController1.m
//  ZZR
//
//  Created by ZZR on 2021/10/7.
//

#import "LogoutViewController1.h"
#import "LeePlaceholderTextView.h"
#import "LogoutViewController2.h"

@interface LogoutViewController1 ()<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet LeePlaceholderTextView *textView;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;

@property (nonatomic, strong) UIImageView *selImage;
@property (nonatomic, assign) NSInteger selIndex;
@end

@implementation LogoutViewController1

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = UIColorFromRGB(0xF6F7F9);
    self.navigationItem.title = LocalizedString(@"注销账号");

    self.textView.placeholder = LocalizedString(@"请输入~");
    self.textView.placeholderColor = UIColorFromRGB(0x999999);
    self.textView.delegate = self;
}


#pragma mark - 💓💓 Network ------

#pragma mark - 💓💓 Delegate ------
#pragma mark - DataSourceAndDelegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    NSString * toBeString = [textView.text stringByReplacingCharactersInRange:range withString:text];

    //得到输入框的内容
    if (toBeString.length > 200) {
        NSString *str = [toBeString substringToIndex:200];
        textView.text = str;
        return NO;
    }
    return YES;
}
#pragma mark - 💓💓 Function ------

#pragma mark - 💓💓 XibFunction ------

- (IBAction)buttonClick:(UIButton *)sender {
    self.selIndex = sender.tag;
    self.selImage.image = [UIImage imageNamed:@"路径 19203"];;
    UIImageView *imageView = sender.superview.subviews.lastObject;
    self.selImage = imageView;
    self.selImage.image = [UIImage imageNamed:@"路径 5442"];;
}

- (IBAction)nextButtonClick:(id)sender {
    if (self.selIndex == 0) {
        [LJTools showText:LocalizedString(@"请选择注销账号的原因") delay:1];
        return;
    }

    NSString *reason = nil;
    if (self.selIndex == 1) {
        reason = LocalizedString(@"多账号，释放邮箱号");
    }
    else if (self.selIndex == 2) {
        reason = LocalizedString(@"不想使用了");
    }
    else if (self.selIndex == 3) {
        reason = LocalizedString(@"其他");
    }
    
    LogoutViewController2 *vc = [[LogoutViewController2 alloc] init];
    vc.selIndex = self.selIndex;
    vc.reason = self.textView.text;
//    vc.reason = reason;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - 💓💓 Lazy Loads ------

@end
