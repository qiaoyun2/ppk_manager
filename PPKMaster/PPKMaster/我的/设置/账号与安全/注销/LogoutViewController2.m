//
//  LogoutViewController2.m
//  ZZR
//
//  Created by ZZR on 2021/10/7.
//

#import "LogoutViewController2.h"
#import "LogoutViewController3.h"

@interface LogoutViewController2 ()

@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UILabel *hintLabel;


@end

@implementation LogoutViewController2

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xF6F7F9);
    self.navigationItem.title = LocalizedString(@"注销账号");
    [self requestForConfig];
}


#pragma mark - 💓💓 Network ------

/** 注销账号-1 **/

- (void) LogoutMethod{
    NSDictionary *params = @{
        @"delType":@(self.selIndex),
        @"delReason":self.reason,
    };
    WeakSelf;
    [NetworkingTool postWithUrl:@"" params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            LogoutViewController3 *vc = [[LogoutViewController3 alloc] init];
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }else
        {
            [LJTools showNOHud:responseObject[@"msg"] delay:1];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools hideHud];
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}


- (void)requestForConfig
{
    [NetworkingTool getWithUrl:kConfigNameURL params:@{@"configName":@"delUserRemind"} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            self.hintLabel.text = responseObject[@"data"];
        }else {
            
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

#pragma mark - 💓💓 Delegate ------
#pragma mark - DataSourceAndDelegate

#pragma mark - 💓💓 Function ------

#pragma mark - 💓💓 XibFunction ------

- (IBAction)backButtonClick:(UIButton *)sender {
    [self backItemClicked];
}

- (IBAction)nextButtonClick:(id)sender {
    MJWeakSelf;
    [UIAlertController alertViewNormalWithTitle:@"提示" message:@"请再次确认，你已了解注销风险并要继续执行该操作" titlesArry:@[@"确认"] indexBlock:^(NSInteger index, id obj) {
        if (index == 2) {
            // 确定
            [weakSelf LogoutMethod];
        }
    } okColor:[UIColor redColor] cancleColor:[UIColor lightGrayColor] isHaveCancel:YES];
}

#pragma mark - 💓💓 Lazy Loads ------

@end
