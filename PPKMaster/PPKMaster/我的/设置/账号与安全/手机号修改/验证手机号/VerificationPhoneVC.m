//
//  VerificationPhoneVC.m
//  ZZR
//
//  Created by null on 2020/9/11.
//  Copyright © 2020 null. All rights reserved.
//

#import "VerificationPhoneVC.h"
#import "ChangePasswordVC.h"
#import "ChangePhoneVC.h"
#import "HLCircleCodeView.h"
#import "ChangePayPasswordVC.h" // 修改支付密码

@interface VerificationPhoneVC ()

{
    NSString * realMobile;
}

@property (weak, nonatomic) IBOutlet UIButton *codeBtn;

@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;

@property (weak, nonatomic) IBOutlet UIView *bView;
@property (nonatomic, strong) HLCircleCodeView *circleCodeView;
@property (nonatomic, strong) NSString *surePwd;// 确认的密码

@end

@implementation VerificationPhoneVC

- (void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"验证手机号";
    self.view.backgroundColor = [UIColor whiteColor];
    User * user = [User getUser];
    self.phoneLabel.text = [NSString stringWithFormat:@"%@",user.mobile];
    [self.bView addSubview:self.circleCodeView];
}

#pragma mark – UI
#pragma mark – Network
#pragma mark – Delegate
#pragma mark - Function

#pragma mark – XibFunction
- (IBAction)btnClick:(id)sender {
    [self.view endEditing:YES];
    if (self.surePwd.length < 4) {
        [LJTools showText:LocalizedString(@"请输入正确验证码") delay:1];
        return;
    }

    if (self.type == 1) {
        [NetworkingTool postWithUrl:kChangeMobileFirstURL params:@{@"event":@"verifyPhone",@"captcha":self.surePwd,@"mobile":[User getUser].mobile} success:^(NSURLSessionDataTask *task, id responseObject) {
            if ([responseObject[@"code"] integerValue]==1) {
                ChangePhoneVC *vc = [[ChangePhoneVC alloc] init];
                [self.navigationController pushViewController:vc animated:YES];
            }else {
                [LJTools showText:responseObject[@"msg"] delay:1.5];
            }
        } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
            
        } IsNeedHub:YES];
        
    }else if(self.type == 2){
        ChangePayPasswordVC *vc = [[ChangePayPasswordVC alloc] init];
        vc.type = 2;
        vc.code = self.surePwd;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        [NetworkingTool postWithUrl:kChangePwdByCodeFirstURL params:@{@"event":@"verifyPhone",@"captcha":self.surePwd} success:^(NSURLSessionDataTask *task, id responseObject) {
            if ([responseObject[@"code"] integerValue]==1) {
                ChangePasswordVC * vc = [[ChangePasswordVC alloc] init];
                vc.type = 1;
                vc.code = self.surePwd;
                [self.navigationController pushViewController:vc animated:YES];
            }else {
                [LJTools showText:responseObject[@"msg"] delay:1.5];
            }
        } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
            
        } IsNeedHub:YES];
    }
}
- (IBAction)sendCode:(UIButton *)sender {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"mobile"] = [User getUser].mobile;
    if (self.type==2) {
        params[@"event"] = @"changePayPwd";
    }else {
        params[@"event"] = @"verifyPhone";
    }
    [NetworkingTool postWithUrl:kGetCodeURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            // 获取验证码
            [sender setCountdown:60 WithStartString:@"" WithEndString:LocalizedString(@"获取验证码")];
            [LJTools showText:@"发送成功" delay:1.5];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}
#pragma mark – lazy load
#pragma mark - 💓💓 Lazy Loads ------
- (HLCircleCodeView *)circleCodeView {
    if (!_circleCodeView) {
        
        _circleCodeView = [[HLCircleCodeView alloc] initWithCount:4 margin:-0.5];
        _circleCodeView.frame = CGRectMake(40, 0, SCREEN_WIDTH-80, 50);
//        _circleCodeView.isSurePwd = YES;
//        [_circleCodeView.textField becomeFirstResponder];
        
        MJWeakSelf;
        _circleCodeView.hl_circleCode = ^(NSString * _Nonnull circleCode) {
            weakSelf.surePwd = circleCode;
            [weakSelf btnClick:nil];
        };
        _circleCodeView.hl_circleRun = ^{
            
        };
    }
    
    return _circleCodeView;
}

@end
