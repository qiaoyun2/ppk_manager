//
//  AboutUsVC.m
//  ZZR
//
//  Created by null on 2021/2/20.
//

#import "AboutUsVC.h"

@interface AboutUsVC ()
@property (weak, nonatomic) IBOutlet UILabel *version;

@end

@implementation AboutUsVC
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.version.text = [NSString stringWithFormat:@"当前版本%@",[LJTools getAppVersion]];
    self.navigationItem.title = @"关于我们";
}

@end
