//
//  MineViewController.m
//  PPKMaster
//
//  Created by null on 2022/4/16.
//

#import "MineViewController.h"
#import "VipCenterController.h"
#import "PersonCertificationController.h"
#import "MyWalletViewController.h"
#import "MyCommentController.h"
#import "SettingController.h"
#import "PersonDataController.h"
#import "AboutUsVC.h"
#import "CertificationFinishController.h"

@interface MineViewController ()
@property (weak, nonatomic) IBOutlet UIView *userInfoView;
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewHeight;
@property (weak, nonatomic) IBOutlet UIView *vipView;
@property (weak, nonatomic) IBOutlet UIView *walletView;

@end

@implementation MineViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    if ([LJTools islogin]) {
        [self requestForUserInfo];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xF5F6F9);
    self.topViewHeight.constant = NavAndStatusHight;
    if ([LJTools getAppDelegate].isOnline) {
        self.vipView.hidden = YES;
        self.walletView.hidden = YES;
    }else {
        self.vipView.hidden = NO;
        self.walletView.hidden = NO;
    }
    WeakSelf
    [self.userInfoView jk_addTapActionWithBlock:^(UIGestureRecognizer *gestureRecoginzer) {
        PersonDataController *vc = [[PersonDataController alloc] init];
        [weakSelf.navigationController pushViewController:vc animated:YES];
    }];
}

#pragma mark - Network
- (void)requestForUserInfo
{
    [NetworkingTool getWithUrl:kGetUserInfoURL params:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            User *user = [User mj_objectWithKeyValues:responseObject[@"data"]];
            [User saveUser:user];
            [self.headImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(user.avatar)] placeholderImage:DefaultImgHeader];
            self.nameLabel.text = user.user_nickname;
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:NO];
}

#pragma mark - XibFunction
- (IBAction)buttonsAction:(UIButton *)sender {
    switch (sender.tag) {
        case 0:
        {
            // 我的会员
            VipCenterController *vc = [[VipCenterController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
            
        case 1:
        {
            if ([User getUser].authMasterStatus.integerValue==2) {
                CertificationFinishController *vc = [[CertificationFinishController alloc] init];
                [self.navigationController pushViewController:vc animated:YES];
            }
            else if ([User getUser].authMasterStatus.integerValue==1) {
                [LJTools showText:@"您已提交认证，请耐心等待" delay:1.5];
            }
            else {
                // 我的认证
                PersonCertificationController *vc = [[PersonCertificationController alloc] init];
                [self.navigationController pushViewController:vc animated:YES];
                if ([User getUser].authMasterStatus.integerValue==3) {
                    [LJTools showText:@"您的认证被拒绝，请重新提交" delay:1.5];
                }
            }
        }
            break;
            
        case 2:
        {
            // 我的钱包
            MyWalletViewController *vc = [[MyWalletViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;

        case 3:
        {
            // 我的评价
            MyCommentController *vc = [[MyCommentController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;

        case 4:
        {
            // 关于我们
            AboutUsVC *vc = [[AboutUsVC alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
            
        case 5:
        {
            // 接单须知
            [NetworkingTool getWithUrl:kConfigNameURL params:@{@"configName":@"masterReceiveRemind"} success:^(NSURLSessionDataTask *task, id responseObject) {
                [LJTools hideHud];
                if ([responseObject[@"code"] intValue] == SUCCESS) {
                    NSString *privacy = (NSString *)responseObject[@"data"];
                    WKWebViewController *vc = [WKWebViewController new];
                    vc.titleStr = @"接单须知";
                    vc.contentStr = privacy;
                    [self.navigationController pushViewController:vc animated:YES];
                } else {
                    [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
                }
            } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
                [LJTools showNOHud:RequestServerError delay:1.0];
            } IsNeedHub:YES];
        }
            break;
            
        case 6:
        {
            // 设置
            SettingController *vc = [[SettingController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
            
        default:
            break;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
