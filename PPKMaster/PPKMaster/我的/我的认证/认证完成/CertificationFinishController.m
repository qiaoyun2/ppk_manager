//
//  CertificationFinishController.m
//  PPK
//
//  Created by null on 2022/3/12.
//

#import "CertificationFinishController.h"
#import "PersonCertificationController.h"

@interface CertificationFinishController ()

@end

@implementation CertificationFinishController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"认证完成";
}

- (IBAction)bottomButtonAction:(UIButton *)sender {
    if (sender.tag==0) {
        self.tabBarController.selectedIndex = 0;
        [self.navigationController popViewControllerAnimated:NO];
    }
    else {
        PersonCertificationController *vc = [[PersonCertificationController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
