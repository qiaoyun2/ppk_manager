//
//  PersonDataController.m
//  ZZR
//
//  Created by null on 2020/12/3.
//

#import "PersonDataController.h"
#import "YBImageBrowser.h"
#import "LJImagePicker.h"
#import "UploadManager.h"
#import "UIAlertController+Category.h"
#import "LoginViewController.h"
#import "NavigationController.h"


@interface PersonDataController ()<UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *mobileLabel;

@property (nonatomic, strong) NSString *imagePath;
@property (nonatomic, strong) NSString *imageId;

@end

@implementation PersonDataController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    User *user = [User getUser];
    self.imagePath = user.avatar;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(self.imagePath)] placeholderImage:DefaultImgHeader];
    self.nameLabel.text = user.user_nickname;
    self.mobileLabel.text = user.mobile;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"我的资料";
    self.view.backgroundColor = UIColorFromRGB(0xF6F7F9);
    [self setNavigationRightBarButtonWithTitle:@"保存"];
}

#pragma mark - network
- (void)requestForSaveUserInfo
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"nickname"] = self.nameLabel.text;
    params[@"avatar"] = self.imagePath;
    [NetworkingTool postWithUrl:kEditUserInfoURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            User *user = [User getUser];
            user.avatar = self.imagePath;
            user.user_nickname = self.nameLabel.text;
            [User saveUser:user];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

- (void)requestForDeleteAccount
{
    [NetworkingTool postWithUrl:kDelUserURL params:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [User deleUser];
            [UIApplication sharedApplication].keyWindow.rootViewController = [[NavigationController alloc] initWithRootViewController:[[LoginViewController alloc] init]];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

- (void)rightButtonTouchUpInside:(UIBarButtonItem *)sender
{
    [self requestForSaveUserInfo];
}

#pragma mark - Xib
- (IBAction)avatarTap:(UITapGestureRecognizer *)sender {
    WeakSelf
    [LJImagePicker showImagePickerFromViewController:self allowsEditing:YES finishAction:^(UIImage *image) {
        if (image) {
            [UploadManager uploadImageArray:@[image] block:^(NSString * _Nonnull ids, NSString * _Nonnull imageUrl) {
                [weakSelf.headImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(imageUrl)] placeholderImage:DefaultImgHeader];
                weakSelf.imagePath = imageUrl;
            }];
        }
    }];
}

- (IBAction)nickNameTap:(UITapGestureRecognizer *)sender {
    WeakSelf
    [UIAlertController addtextFeildWithTitle:@"修改昵称" message:nil indexBlock:^(NSInteger index, NSString *obj) {
        if (obj.length==0 || obj.length>10) {
            [LJTools showText:@"请输入1-10位昵称" delay:1.5];
            return;
        }
        weakSelf.nameLabel.text = obj;
    }];
}

//- (IBAction)privacyButtonAction:(UIButton *)sender {
//    /// 隐私政策
//    [NetworkingTool getWithUrl:kConfigNameURL params:@{@"configName":@"privacyPolicy"} success:^(NSURLSessionDataTask *task, id responseObject) {
//        [LJTools hideHud];
//        if ([responseObject[@"code"] intValue] == SUCCESS) {
//            NSString *privacy = (NSString *)responseObject[@"data"];
//            WKWebViewController *vc = [WKWebViewController new];
//            vc.titleStr = @"隐私政策";
//            vc.urlStr = privacy;
//            [self.navigationController pushViewController:vc animated:YES];
//        } else {
//            [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
//        }
//    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
//        [LJTools showNOHud:RequestServerError delay:1.0];
//    } IsNeedHub:YES];
//}
//
//- (IBAction)agreementButtonAction:(UIButton *)sender {
//    [NetworkingTool getWithUrl:kConfigNameURL params:@{@"configName":@"registerAgreement"} success:^(NSURLSessionDataTask *task, id responseObject) {
//        [LJTools hideHud];
//        if ([responseObject[@"code"] intValue] == SUCCESS) {
//            NSString *agreement = (NSString *)responseObject[@"data"];
//            WKWebViewController *vc = [WKWebViewController new];
//            vc.titleStr = @"用户注册协议";
//            vc.urlStr = agreement;
//            [self.navigationController pushViewController:vc animated:YES];
//        } else {
//            [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
//        }
//    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
//        [LJTools showNOHud:RequestServerError delay:1.0];
//    } IsNeedHub:YES];
//}
//
//- (IBAction)deleteAccountButtonAction:(UIButton *)sender {
//    WeakSelf
//    [UIAlertController alertViewNormalWithTitle:@"温馨提示" message:@"确定要注销账号吗？" titlesArry:@[@"确定"] indexBlock:^(NSInteger index, id obj) {
//        [weakSelf requestForDeleteAccount];
//    } okColor:[UIColor redColor] cancleColor:[UIColor grayColor] isHaveCancel:YES];
//
//}


@end
