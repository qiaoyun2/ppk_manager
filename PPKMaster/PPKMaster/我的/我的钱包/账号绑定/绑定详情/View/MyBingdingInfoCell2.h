//
//  MyBingdingInfoCell2.h
//  ZZR
//
//  Created by null on 2021/3/16.
//  Copyright © 2021 null. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyBingdingInfoCell2 : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *addImg;
@property (nonatomic, copy) void (^didChooseImage)(UIImage *image);

@end

NS_ASSUME_NONNULL_END
