//
//  MyBingdingInfoCell1.h
//  ZZR
//
//  Created by null on 2021/3/16.
//  Copyright © 2021 null. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyBingdingInfoCell1 : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *contentL;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIButton *sendBtn;

@end

NS_ASSUME_NONNULL_END
