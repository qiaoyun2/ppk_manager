//
//  WithdrawListModel.h
//  StarCard
//
//  Created by ZZR on 2022/4/14.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface WithdrawListModel : NSObject
//remark    String[字符串]    备注
//is_auto    String[字符串]    转账 0 手动 1自动
//transfer_img    String[字符串]    转账截图
//transfer_id    String[字符串]    转账账户id
//transaction_id    String[字符串]    第三方订单号
//pay_fee    String[字符串]    到账金额
//handling_fee    String[字符串]    手续费
//cash_fee    String[字符串]    提现金额
//account_id    String[字符串]    提现账户 微信UNIONID 支付宝账户 银行卡号
//account_type    String[字符串]    账户类型 1 微信 2 支付宝 3 银行卡
//cash_time    String[字符串]    付款时间
//check_time    String[字符串]    审核时间
//cash_reason    String[字符串]    付款异常原因
//check_reason    String[字符串]    审核失败原因
//cash_status    String[字符串]    付款状态
//check_status    String[字符串]    审核状态
//true_name    String[字符串]    真实姓名
//create_time    String[字符串]    申请时间
//order_no    String[字符串]    提现流水号
//user_id    String[字符串]    会员ID
//id    String[字符串]    文档id
@property (nonatomic, strong) NSNumber *account_type;
@property (nonatomic, strong) NSNumber *cash_time;
@property (nonatomic, strong) NSNumber *user_id;
@property (nonatomic, copy) NSString *order_no;
@property (nonatomic, copy) NSString *create_time;
@property (nonatomic, copy) NSString *transaction_id;
@property (nonatomic, copy) NSString *is_auto;
@property (nonatomic, copy) NSString *handling_fee;
@property (nonatomic, copy) NSString *transfer_img;
@property (nonatomic, copy) NSString *cash_fee;
@property (nonatomic, strong) NSNumber *id;
@property (nonatomic, strong) NSNumber *cash_status;
@property (nonatomic, strong) NSNumber *check_status;
@property (nonatomic, copy) NSString *pay_fee;
@property (nonatomic, copy) NSString *true_name;
@property (nonatomic, copy) NSString *cash_reason;
@property (nonatomic, copy) NSString *account_id;
@property (nonatomic, strong) NSNumber *check_time;
@property (nonatomic, strong) NSNumber *transfer_id;
@property (nonatomic, copy) NSString *check_reason;
@property (nonatomic, copy) NSString *remark;


/// 自己扩展
/// 状态为提现的时候 0:待审核 1:已到账 2:已拒绝
@property (nonatomic, copy) NSString *check_status_name;



@end
//"account_id" = 1;
//"account_type" = 2;
//"cash_fee" = "14.00";
//"cash_reason" = "";
//"cash_status" = 0;
//"cash_time" = 0;
//"check_reason" = "不予";
//"check_status" = 2;
//"check_time" = 1649930006;
//"create_time" = "2022-04-14 17:52:09";
//"handling_fee" = "0.70";
//id = 2;
//"is_auto" = 0;
//"order_no" = TX20220414175209062277;
//"pay_fee" = "13.30";
//remark = "";
//"transaction_id" = "";
//"transfer_id" = 0;
//"transfer_img" = "";
//"true_name" = "许可乐";
//"user_id" = 4;
NS_ASSUME_NONNULL_END
