//
//  WithdrawViewController.h
//  ZZR
//
//  Created by null on 2021/9/13.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@class MyWalletModel, BindingStatusModel;

@interface WithdrawViewController : BaseViewController

@property (nonatomic, strong) MyWalletModel *myWallerModel;
@property (nonatomic, strong) BindingStatusModel *statusModel;

@end

NS_ASSUME_NONNULL_END
