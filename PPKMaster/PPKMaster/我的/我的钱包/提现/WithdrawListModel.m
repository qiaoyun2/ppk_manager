//
//  WithdrawListModel.m
//  StarCard
//
//  Created by ZZR on 2022/4/14.
//

#import "WithdrawListModel.h"

@implementation WithdrawListModel

- (void)setCheck_status:(NSNumber *)check_status{

    _check_status = check_status;
    switch (check_status.intValue) {
        case 0:
            self.check_status_name = @"审核中";
            break;
        case 1:
            self.check_status_name = @"已到账";
            break;
        case 2:
            self.check_status_name = @"审核驳回";
            break;
            
        default:
            break;
    }
}

@end
