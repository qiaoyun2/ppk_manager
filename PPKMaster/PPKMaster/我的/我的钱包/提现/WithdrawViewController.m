//
//  WithdrawViewController.m
//  ZZR
//
//  Created by null on 2021/9/13.
//

#import "WithdrawViewController.h"
#import "WithdrawStatusViewController.h"
#import "MyBingdingInfoVC.h"
#import "WithdrawDetailsController.h"
#import "MyWalletModel.h"
#import "BindingStatusModel.h"


@interface WithdrawViewController ()

/// 账户余额
@property (weak, nonatomic) IBOutlet UITextField *amountTextField;
/// 比例
@property (weak, nonatomic) IBOutlet UILabel *scaleLabel;

/// 提现方式
@property (weak, nonatomic) IBOutlet UIButton *alipayItem;
@property (weak, nonatomic) IBOutlet UIButton *wechatItem;

@property (weak, nonatomic) IBOutlet UIButton *withdrawButton;

@property (nonatomic, strong) UIButton *selectedButton;


@end

@implementation WithdrawViewController

#pragma mark - Life Cycle Methods

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.title = @"提现";
    self.view.backgroundColor = UIColorNamed(@"F6F7F9");
    [self setNavigationRightBarButtonWithTitle:@"明细" color:UIColorFromRGB(0x333333)];
    [_amountTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
//    [_amountTextField setPlaceholder:[@"账户余额" stringByAppendingString:_myWallerModel.user_money]];
//    [_scaleLabel setText:[NSString stringWithFormat:@"提现收取%@%%的手续费", _myWallerModel.withdraw_handling_fee]];
    if (!_statusModel.is_alipay && _statusModel.is_wx) {
        UIButton *wechat = [self.view viewWithTag:901];
        [self chooseWayAction:wechat];
    } else if (!_statusModel.is_alipay && !_statusModel.is_wx && _statusModel.is_bank) {
        UIButton *bank = [self.view viewWithTag:902];
        [self chooseWayAction:bank];
    } else {
        UIButton *alipay = [self.view viewWithTag:900];
        [self chooseWayAction:alipay];
    }
}

#pragma mark – UI
#pragma mark - Network

- (void)getConfigData {
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    WeakSelf;
    [NetworkingTool postWithUrl:@"" params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            NSString *content = responseObject[@"data"][@"content"];
        }else
        {
            [LJTools showNOHud:responseObject[@"msg"] delay:1];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools hideHud];
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}

#pragma mark - Delegate
#pragma mark - UITableViewDelgate
#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (range.length >= 1) {
        /// 删除数据, 都允许删除
        return YES;
    }
    NSString *money = [textField.text stringByAppendingString:string];
    if (![money validateMoney]) {
        if (textField.text.length > 0 && [string isEqualToString:@"."] && ![textField.text containsString:@"."]) {
            return YES;
        }
        return NO;
    }
    return YES;
//    return money.floatValue <= _myWallerModel.user_money.floatValue;
}

#pragma mark - Function

- (void)rightButtonTouchUpInside:(UIBarButtonItem *)sender {
    WithdrawDetailsController *withdrawDetailsVC = [WithdrawDetailsController new];
    [self.navigationController pushViewController:withdrawDetailsVC animated:YES];
}

/// 输入框观察者事件
- (void)textFieldDidChange:(UITextField *)textField {
    if (textField == _amountTextField && _amountTextField.text.length > 12) {
        _amountTextField.text = [_amountTextField.text substringToIndex:12];
    }
}

#pragma mark – XibFunction
- (IBAction)operationAction:(UIButton *)sender {
    switch (sender.tag) {
        case 801:
        {
            /// 全部
//            _amountTextField.text = _myWallerModel.user_money;
        }
            break;
        case 802:
        {
            /// 疑问
            [self getConfigData];

        }
            break;
        case 803:
        {
            /// 提现
            if ([_amountTextField.text integerValue] <= 0) {
                [LJTools showNOHud:@"请输入提现金额" delay:1];
                return;
            }
            
            NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
            parameters[@"money"] = _amountTextField.text;
            if (self.selectedButton.tag == 900) {
                parameters[@"type"] = @"2";
            } else if (self.selectedButton.tag == 901) {
                parameters[@"type"] = @"1";
            } else {
                parameters[@"type"] = @"3";
            }
            
            [NetworkingTool postWithUrl:@"" params:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
                [LJTools hideHud];
                if ([responseObject[@"code"] intValue] == SUCCESS) {
                    WithdrawStatusViewController *statusVC = [WithdrawStatusViewController new];
                    statusVC.withdrawType = [parameters[@"type"] integerValue];
                    statusVC.money = self.amountTextField.text;
                    [self.navigationController pushViewController:statusVC animated:YES];
                } else {
                    [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
                }
            } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
                [LJTools showNOHud:RequestServerError delay:1.0];
            } IsNeedHub:YES];
        }
            break;
            
        default:
            break;
    }
}

/// 选择提现方式
/// @param sender <#sender description#>
- (IBAction)chooseWayAction:(UIButton *)sender {
    if (sender.isSelected) {
        return;
    }
//    if (sender.tag == 900 && !_statusModel.is_alipay) {
//        MyBingdingInfoVC *info = [[MyBingdingInfoVC alloc] init];
//        info.state = 0;
//        [self.navigationController pushViewController:info animated:YES];
//        return;
//    }
//    if (sender.tag == 901 && !_statusModel.is_wx) {
//        MyBingdingInfoVC *info = [[MyBingdingInfoVC alloc] init];
//        info.state = 1;
//        [self.navigationController pushViewController:info animated:YES];
//        return;
//    }

    sender.selected = YES;
    _selectedButton.selected = NO;
    _selectedButton = sender;
    _alipayItem.selected = sender.tag == 900;
    _wechatItem.selected = sender.tag == 901;
}

#pragma mark - Lazy Loads

@end
