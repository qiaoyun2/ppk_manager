//
//  WithdrawStatusViewController.h
//  ZZR
//
//  Created by null on 2021/9/14.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

/** WithdrawStatusViewController 提现申请成功 */
@interface WithdrawStatusViewController : BaseViewController

/// 提现方式 1:微信 2:支付宝 3:银行卡
@property (nonatomic, assign) NSInteger withdrawType;
/// 金额
@property (nonatomic, copy) NSString *money;

@end

NS_ASSUME_NONNULL_END
