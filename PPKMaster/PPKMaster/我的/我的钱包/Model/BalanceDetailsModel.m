//
//  BalanceDetailsModel.m
//  ZZR
//
//  Created by null on 2021/12/22.
//

#import "BalanceDetailsModel.h"

@implementation BalanceDetailsModel

- (void)setCheck_status:(NSInteger)check_status {
    _check_status = check_status;
    switch (check_status) {
        case 0:
            self.check_status_name = @"审核中";
            break;
        case 1:
            self.check_status_name = @"已到账";
            break;
        case 2:
            self.check_status_name = @"审核驳回";
            break;
            
        default:
            break;
    }
}

@end
