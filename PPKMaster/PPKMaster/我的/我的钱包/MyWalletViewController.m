//
//  MyWalletViewController.m
//  ZZR
//
//  Created by null on 2021/11/22.
//

#import "MyWalletViewController.h" //钱包
#import "CoinBillFlowCell.h"

#import "WithdrawViewController.h"
#import "MyBindingVC.h"
#import "BalanceDetailsViewController.h"

#import "MyWalletModel.h"
#import "BindingStatusModel.h"


@interface MyWalletViewController ()

@property (weak, nonatomic) IBOutlet UILabel *todayIncomeLabel; // 今日收入
@property (weak, nonatomic) IBOutlet UILabel *withdrawMoneyLabel; // 可提现金额
/// 累计收入
@property (weak, nonatomic) IBOutlet UILabel *allIncomeLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewHeight;

@property (nonatomic, strong) MyWalletModel *myWallerModel;
@property (nonatomic, strong) BindingStatusModel *statusModel;

@property (nonatomic, assign) NSInteger page;

@end

@implementation MyWalletViewController

#pragma mark - Life Cycle Methods

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];

    [self getMyWalletInfo];
//    [self getBindingStatus];
    [self refresh];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xF5F6F9);
    self.topViewHeight.constant = NavAndStatusHight;
    [self.tableView registerNib:[UINib nibWithNibName:@"CoinBillFlowCell" bundle:nil] forCellReuseIdentifier:@"CoinBillFlowCell"];
    WeakSelf
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^(void) {
        [weakSelf refresh];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMore];
    }];
    
}

#pragma mark - Network
- (void)refresh {
    _page = 1;
    [self getDataArrayFromServerIsRefresh:YES];
}
- (void)loadMore {
    _page++;
    [self getDataArrayFromServerIsRefresh:NO];
}

- (void)getDataArrayFromServerIsRefresh:(BOOL)isRefresh {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
    params[@"pageNo"] = @(_page);
    params[@"pageSize"] = @(10);
    params[@"endTime"] = [[NSDate date] jk_formatYMD];
    params[@"startTime"] = [[[NSDate date] jk_dateAfterDay:-30] jk_formatYMD];
//    params[@"startTime"] = @"2022-03-25";
//    params[@"endTime"] = @"2022-04-24";
    WeakSelf
    [NetworkingTool getWithUrl:kFlowRecordURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            if (isRefresh) {
                [self.dataArray removeAllObjects];
            }
            NSArray *tempList = [BalanceDetailsModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"][@"records"]];
            [self.dataArray addObjectsFromArray:tempList];
        }
        else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [self addBlankOnView:self.tableView];
        self.noDataView.hidden = weakSelf.dataArray.count != 0;
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [self.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}



/// 获取我的钱包信息
- (void)getMyWalletInfo {
    [NetworkingTool getWithUrl:kMyWalletURL params:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            self.myWallerModel = [MyWalletModel mj_objectWithKeyValues:responseObject[@"data"]];
            self.todayIncomeLabel.text = [NSString stringWithFormat:@"%.2f",[self.myWallerModel.todayIncome floatValue]];
            self.withdrawMoneyLabel.text = [NSString stringWithFormat:@"%.2f",[self.myWallerModel.withdrawMoney floatValue]];
            self.allIncomeLabel.text = [NSString stringWithFormat:@"%.2f",[self.myWallerModel.totalIncome floatValue]];
        } else {
            [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}

/// 获取绑定账号状态
- (void)getBindingStatus {
    [NetworkingTool postWithUrl:@"" params:@{} success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            self.statusModel = [BindingStatusModel mj_objectWithKeyValues:responseObject[@"data"]];
        } else {
            [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}

#pragma mark - Delegate
#pragma mark UITableViewDelgate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CoinBillFlowCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CoinBillFlowCell" forIndexPath:indexPath];
    if (indexPath.row < self.dataArray.count) {
        cell.detailsModel = self.dataArray[indexPath.row];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

#pragma mark – Function

#pragma mark – XibFunction
- (IBAction)withdrawAction:(id)sender {
//    if (!_statusModel.is_alipay && !_statusModel.is_wx && !_statusModel.is_bank) {
//        MyBindingVC *bindingVC = [MyBindingVC new];
//        [self.navigationController pushViewController:bindingVC animated:YES];
//        return;
//    }
    WithdrawViewController *withdrawVC = [WithdrawViewController new];
    withdrawVC.myWallerModel = _myWallerModel;
    withdrawVC.statusModel = _statusModel;
    [self.navigationController pushViewController:withdrawVC animated:YES];
}

/// 全部记录
/// @param sender <#sender description#>
- (IBAction)allRecordAction:(id)sender {
    BalanceDetailsViewController *banlanceDetailsVC = [BalanceDetailsViewController new];
    [self.navigationController pushViewController:banlanceDetailsVC animated:YES];
}

- (IBAction)bindAccountButtonAction:(UIButton *)sender {
    MyBindingVC *bindingVC = [MyBindingVC new];
    [self.navigationController pushViewController:bindingVC animated:YES];
}

- (IBAction)backButtonAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark - Lazy Loads

@end
