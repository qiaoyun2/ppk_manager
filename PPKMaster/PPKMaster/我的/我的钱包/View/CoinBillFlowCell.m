//
//  CoinBillFlowCell.m
//  ZZR
//
//  Created by null on 2021/9/11.
//

#import "CoinBillFlowCell.h"

@implementation CoinBillFlowCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setDetailsModel:(BalanceDetailsModel *)detailsModel {
    _detailsModel = detailsModel;
    [_titleLabel setText:detailsModel.title];
    [_timeLabel setText:detailsModel.createTime];
    BOOL isAdd = [detailsModel.sign isEqual:@"+"];
    [_moneyLabel setText:[detailsModel.sign stringByAppendingString:detailsModel.change_money]];
    [_moneyLabel setTextColor:isAdd ? UIColorNamed(@"FA2033") : UIColorNamed(@"333333")];
//    self.moneyLabel.attributedText = [detailsModel.change_money attributedWithAdd:isAdd bigFont:16 smallFont:11];
}



- (void)defaultValue {
    [_titleLabel setText:@"订单结算入账，当前余额增加"];
    [_timeLabel setText:@"2020-05-12 14:29"];
}

@end
