//
//  BalanceDetailsListCell.m
//  ZZR
//
//  Created by null on 2021/9/14.
//

#import "BalanceDetailsListCell.h"

@implementation BalanceDetailsListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setDetailsModel:(BalanceDetailsModel *)detailsModel {
    _detailsModel = detailsModel;
    
    [_titleLabel setText:detailsModel.remark];
    [_timeLabel setText:detailsModel.create_time];
    BOOL isAdd = ![detailsModel.change_money hasPrefix:@"-"];
    NSString *symbol = isAdd ? @"+" : @"";
    [_moneyLabel setText:[symbol stringByAppendingString:detailsModel.change_money]];
    [_moneyLabel setTextColor:isAdd ? UIColorNamed(@"333333") : UIColorNamed(@"999999")];
    self.moneyLabel.attributedText = [detailsModel.change_money attributedWithAdd:isAdd bigFont:16 smallFont:11];

    [_statusLabel setText:detailsModel.check_status_name];

    
}

- (void)setWModel:(WithdrawListModel *)wModel{
    _wModel = wModel;
    [_titleLabel setText:@"余额提现"];
    [_timeLabel setText:wModel.create_time];
    BOOL isAdd = NO;


    [_moneyLabel setTextColor:isAdd ? UIColorNamed(@"333333") : UIColorNamed(@"999999")];
    self.moneyLabel.attributedText = [wModel.cash_fee attributedWithAdd:isAdd bigFont:16 smallFont:11];

    [_statusLabel setText:wModel.check_status_name];

}

- (void)setIsWithdraw:(BOOL)isWithdraw {
    _isWithdraw = isWithdraw;
    _statusLabel.hidden = NO;
}

@end
