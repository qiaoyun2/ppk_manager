//
//  BalanceDetailsListCell.h
//  ZZR
//
//  Created by null on 2021/9/14.
//

#import <UIKit/UIKit.h>
#import "BalanceDetailsModel.h"
#import "WithdrawListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BalanceDetailsListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

@property (nonatomic, assign) BOOL isWithdraw;

@property (nonatomic, strong) BalanceDetailsModel *detailsModel;
@property (nonatomic, strong) WithdrawListModel *wModel;

@end

NS_ASSUME_NONNULL_END
