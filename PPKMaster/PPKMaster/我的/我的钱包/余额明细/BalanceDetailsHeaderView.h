//
//  BalanceDetailsHeaderView.h
//  ZZR
//
//  Created by null on 2021/9/14.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BalanceDetailsHeaderView : UITableViewHeaderFooterView

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (nonatomic, copy) dispatch_block_t closeCallBack;

@end

NS_ASSUME_NONNULL_END
