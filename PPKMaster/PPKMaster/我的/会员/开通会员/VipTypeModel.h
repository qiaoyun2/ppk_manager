//
//  VipTypeModel.h
//  PPK
//
//  Created by null on 2022/3/14.
//

#import "BaseModel.h"


NS_ASSUME_NONNULL_BEGIN

@interface VipTypeModel : BaseModel

@property (nonatomic, strong) NSString *effectiveDay; // 非次卡的有效天数
@property (nonatomic, strong) NSString *ID; //
@property (nonatomic, strong) NSString *picture; // 背景图片
@property (nonatomic, strong) NSString *price; // 价格
@property (nonatomic, strong) NSString *releaseNumber; // 发布次数
@property (nonatomic, strong) NSString *remarks; // 备注
@property (nonatomic, strong) NSString *title; // vip类型：次卡，季卡，年卡
@property (nonatomic, strong) NSString *topNumber; // 次卡：置顶次数
@property (nonatomic, strong) NSString *type; // vip类型：1次卡，2季卡，3年卡

@property (nonatomic, strong) NSString *productId; // 内购id

@end

NS_ASSUME_NONNULL_END
