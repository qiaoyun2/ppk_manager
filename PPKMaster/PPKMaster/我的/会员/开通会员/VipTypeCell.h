//
//  VipTypeCell.h
//  PPK
//
//  Created by null on 2022/3/12.
//

#import <UIKit/UIKit.h>
#import "VipTypeModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface VipTypeCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *cornerView;
@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *introduceLabel;
@property (weak, nonatomic) IBOutlet UIView *coverView;

@property (nonatomic, strong) VipTypeModel *model;

@end

NS_ASSUME_NONNULL_END
