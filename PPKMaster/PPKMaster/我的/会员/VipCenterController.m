//
//  VipCenterController.m
//  PPK
//
//  Created by null on 2022/3/12.
//

#import "VipCenterController.h"
#import "PurchaseLogsController.h"
#import "OpenVipView.h"

@interface VipCenterController ()
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *remindTimeLabel; // 90天免费接单
@property (weak, nonatomic) IBOutlet UILabel *vipTypeLabel; // 月卡会员

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewHeight;
@property (weak, nonatomic) IBOutlet UILabel *privilegeLabel; // 特权
@property (weak, nonatomic) IBOutlet UIButton *openButton;

@property (nonatomic, strong) OpenVipView *vipView;

@end

@implementation VipCenterController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    if ([LJTools islogin]) {
        [self requestForUserInfo];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.topViewHeight.constant = NavAndStatusHight;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(paySuccess:) name:@"PaySuccess" object:nil];
    
}

- (void)requestForUserInfo
{
    [NetworkingTool getWithUrl:kGetUserInfoURL params:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            User *user = [User mj_objectWithKeyValues:responseObject[@"data"]];
            [User saveUser:user];
            [self updateSubviews];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:NO];
}

#pragma mark - Function
- (void)updateSubviews
{
    User *user = [User getUser];
    self.nameLabel.text = user.user_nickname;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(user.avatar)] placeholderImage:DefaultImgHeader];
    
    if ([user.isMasterVip integerValue]==1) {
        [self.openButton setImage:[UIImage imageNamed:@"组 35353-1"] forState:UIControlStateNormal];
        self.vipTypeLabel.text = [NSString stringWithFormat:@"您是尊敬的%@",user.vipTypeName];
        if ([user.vipLastDay integerValue]>0) {
            self.remindTimeLabel.text = [NSString stringWithFormat:@"当前剩余：%@天免费接单",user.vipLastDay];
        }
        if ([user.receiveNumber integerValue]>0) {
            self.remindTimeLabel.text = [NSString stringWithFormat:@"当前剩余：%@次免费接单",user.receiveNumber];
        }
    }else {
        [self.openButton setImage:[UIImage imageNamed:@"组 35353"] forState:UIControlStateNormal];
        self.vipTypeLabel.text = @"当前未开通会员";
        self.remindTimeLabel.text = @"开通会员享更多优惠";
    }
}

- (void)paySuccess:(NSNotification *)noti
{
    [self requestForUserInfo];
}

#pragma mark - XibFunction
// 返回
- (IBAction)backButtonAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

// 记录
- (IBAction)logButtonAction:(id)sender {
    PurchaseLogsController *vc = [[PurchaseLogsController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

// 开通vip
- (IBAction)openButtonAction:(id)sender {
    if (!self.vipView) {
        OpenVipView *view = [[[NSBundle mainBundle] loadNibNamed:@"OpenVipView" owner:nil options:nil] firstObject];
        view.frame = [UIScreen mainScreen].bounds;
        [[UIApplication sharedApplication].keyWindow addSubview:view];
        self.vipView = view;
    }
    [self.vipView show];
}

- (IBAction)headImageViewTap:(id)sender {

}


@end
