//
//  User.h
//  HNPartyBuilding
//
//  Created by null on 2018/3/10.
//  Copyright © 2018年 null. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject<NSCoding>






@property (nonatomic, strong) NSString *age; // 年龄
@property (nonatomic, strong) NSString *auditMasterOpinion; // 师傅端认证审核意见
@property (nonatomic, strong) NSString *authMasterStatus; // 认证状态：0未认证；1认证中；2已完成；3审核失败
@property (nonatomic, strong) NSString *authMasterType; // 1:货运师傅；2：维修师傅；3：废品回收师傅
@property (nonatomic, strong) NSString *authMobile; // 实名认证手机号
@property (nonatomic, strong) NSString *avatar;
@property (nonatomic, strong) NSString *backCardId; // 身份证反面
@property (nonatomic, strong) NSString *cardId; // 身份证no
@property (nonatomic, strong) NSString *frontCardId; // 身份证正面

@property (nonatomic, strong) NSString *isMasterVip; // 是否vip
@property (nonatomic, strong) NSString *masterClassifyNames; // 入驻选择一级分类names-回收师傅为空
@property (nonatomic, strong) NSString *masterType; // 0：未认证；1:货运师傅；2：维修师傅；3：废品回收师傅
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, copy) NSString *mobileEncrypt; // 加密手机号
@property (nonatomic, copy) NSString *user_nickname;
@property (nonatomic, copy) NSString *receiveNumber; // 接单次数
@property (nonatomic, copy) NSString *sex; // 性别 0男1
@property (nonatomic, copy) NSString *user_id; /// 用户id
@property (nonatomic, copy) NSString *user_name; // 认证真实姓名
@property (nonatomic, copy) NSString *user_token;
@property (nonatomic, copy) NSString *vipLastDay; // 会员剩余时间
@property (nonatomic, copy) NSString *vipTypeName; // vip类型名称
@property (nonatomic, copy) NSString *workYear; // 工龄
@property (nonatomic, strong) NSString *delFlag; // 是否注销0否1是
@property (nonatomic, strong) NSString *delStatus; // 注销审核状态：0未提交；1待审核；2审核通过；3审核不通过
@property (nonatomic, strong) NSString *delType; // 1.多账号，释放手机号；2.不想使用了；3.其他
@property (nonatomic, strong) NSString *delAuditOptions; //注销审核意见
@property (nonatomic, strong) NSString *delReason; //注销原因


//@property (nonatomic, strong) NSString *auditOpinion;
//@property (nonatomic, strong) NSString *auditCompanyOpinion;
//@property (nonatomic, strong) NSString *authStatus; //实名认证状态：0未认证；1认证中；2已完成，（失败重置为0）
//@property (nonatomic, strong) NSString *authCompanyStatus; //公司认证状态：0未认证；1认证中；2已完成，（失败重置为0）
//@property (nonatomic, strong) NSString *authTypeCompany;  // 公司是否实名认证0否1是
//@property (nonatomic, strong) NSString *authTypePersonal; // 个人是否实名认证0否1是
//@property (nonatomic, strong) NSString *companyAddress; // 公司位置
//@property (nonatomic, strong) NSString *companyCreditCode; // 统一社会信用代码
//@property (nonatomic, strong) NSString *companyLegal; // 公司法人名称
//@property (nonatomic, strong) NSString *companyLicense; // 公司营业执照
//@property (nonatomic, strong) NSString *companyName; // 公司名称
//@property (nonatomic, strong) NSString *companyScale; // 公司规模
//@property (nonatomic, strong) NSString *createBy;
//@property (nonatomic, strong) NSString *createTime;
//
//@property (nonatomic, copy) NSString *nation; // 民族
//@property (nonatomic, copy) NSString *password;
//@property (nonatomic, copy) NSString *passwordSecurityLevel;
//@property (nonatomic, copy) NSString *salt;
//@property (nonatomic, copy) NSString *status;
//@property (nonatomic, copy) NSString *updateBy;
//@property (nonatomic, copy) NSString *updateTime;
//
//@property (nonatomic, copy) NSString *isReleaseVip; // 是否是发布会员
//@property (nonatomic, copy) NSString *vipReleaseTypeName; // 发布会员类型名称
//@property (nonatomic, copy) NSString *releaseNumber; // 发布次数
//@property (nonatomic, copy) NSString *vipReleaseLastDay; // 发布会员剩余时间
//
//@property (nonatomic, copy) NSString *isTopVip; // 是否是置顶会员
//@property (nonatomic, copy) NSString *vipTopTypeName; // 置顶会员类型名称
//@property (nonatomic, copy) NSString *topNumber; // 置顶次数
//@property (nonatomic, copy) NSString *vipTopLastDay; // 置顶会员剩余时间
//@property (nonatomic, copy) NSString *address;
//@property (nonatomic, copy) NSString *address_code;

/// 服务器获取用户信息
/// @param callBack <#callBack description#>
+ (void)getUserFromServer:(void (^)(User *user))callBack;
+ (void)getUserFromServer:(NSString *)userId callBack:(void (^)(User *user))callBack;

+ (void)saveUser:(User *)user;
+ (User *)getUser;
+ (void)deleUser;
+ (NSString *)getUserID;
+ (NSString *)getUserToken;
+ (NSString *)getMobile;
+ (NSString *)getMobile2;
+ (NSString *)getUserNickname;
+ (NSString *)getHeader;
+ (NSInteger)getSex;

@end
