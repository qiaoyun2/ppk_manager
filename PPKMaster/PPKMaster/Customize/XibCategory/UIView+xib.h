//
//  UIView+xib.h
//  ZZR
//
//  Created by null on 2020/8/4.
//  Copyright © 2020 null. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (xib)

@property(nonatomic, assign) IBInspectable CGFloat borderWidth;
@property(nonatomic, assign) IBInspectable UIColor *borderColor;
@property(nonatomic, assign) IBInspectable CGFloat cornerRadius;

//shadow
@property(nonatomic, assign) IBInspectable CGSize shadowOffset;
@property(nonatomic, assign) IBInspectable UIColor *shadowColor;
@property(nonatomic, assign) IBInspectable CGFloat shadowOpacity;
@property(nonatomic, assign) IBInspectable CGFloat shadowRadius;
@property(nonatomic, assign) IBInspectable CGFloat cornerShadowRadius;

@end

NS_ASSUME_NONNULL_END
