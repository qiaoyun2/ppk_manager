//
//  FoundListImage9TypographyDetailManager.m
//  LePin
//
//  Created by  ebz on 2020/7/30.
//  Copyright © 2020 ebz. All rights reserved.
//

#import "FoundListImage9TypographyDetailManager.h"
#import <Masonry/Masonry.h>

static CGFloat const fondListImageSpace = 10;

@implementation FoundListImage9TypographyDetailManager

#pragma mark 赋值
+ (void)setValeForImageView:(NSArray *)array imageArray:(NSArray *)imageArray {
    
    // 赋值
    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIImageView *imgV = (UIImageView *)obj;
//        imgV.image = [UIImage imageNamed:imageArray[idx]];
        [imgV sd_setImageWithURL:[NSURL URLWithString:kImageUrl(imageArray[idx])] placeholderImage:[UIImage imageNamed:@"defaultImgWidth"]];
    }];
}
#pragma mark 0个图片
+ (void)zoreImageTypographyImgV1:(UIImageView *)imgV1 currentView:(UIView *)currentView {
    
    [imgV1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(imgV1.mas_width).multipliedBy(0.01);
        make.width.mas_equalTo(currentView.mas_width).multipliedBy(0.7);
        make.top.left.bottom.offset(0);
    }];
}
#pragma mark 1个图片
+ (void)oneImageTypographyImgV1:(UIImageView *)imgV1 currentView:(UIView *)currentView {
    
    [imgV1 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.mas_equalTo(currentView).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
//        make.height.mas_equalTo(163);
        make.height.mas_equalTo(imgV1.mas_width).multipliedBy(0.6);
        make.width.mas_equalTo(currentView.mas_width).multipliedBy(0.7);
        make.top.left.bottom.offset(0);
    }];
}

#pragma mark 2个图片
+ (void)twoImageTypography:(NSArray *)array imgV1:(UIImageView *)imgV1 imgV2:(UIImageView *)imgV2 currentView:(UIView *)currentView {
    // 计算每个item的宽度
    CGFloat tempF = fondListImageSpace/3;
    // 第一排 给imgV1、imgV2赋值
    [imgV1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.offset(0);
        make.width.mas_equalTo(currentView.mas_width).multipliedBy(0.33).offset(-tempF);
        make.height.mas_equalTo(imgV1.mas_width);
        make.bottom.offset(0);
    }];
    
    [imgV2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(imgV1.mas_right).offset(fondListImageSpace);
        make.top.mas_equalTo(imgV1.mas_top);
        make.size.mas_equalTo(imgV1);
    }];
}
#pragma mark 3个图片
+ (void)threeImageTypography:(NSArray *)array imgV1:(UIImageView *)imgV1 {
    [array mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:fondListImageSpace leadSpacing:0.f tailSpacing:0.f];
    
    [array mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(imgV1.mas_width);
        make.top.offset(0);
        make.bottom.offset(0);
    }];
}
#pragma mark 4个图片
+ (void)fourImageTypography:(NSArray *)array imgV1:(UIImageView *)imgV1 imgV2:(UIImageView *)imgV2 imgV3:(UIImageView *)imgV3 imgV4:(UIImageView *)imgV4 currentView:(UIView *)currentView {
    
    // 计算每个item的宽度
    CGFloat tempF = fondListImageSpace/3;
    // 第一排 给imgV1、imgV2赋值
    [imgV1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.offset(0);
        make.width.mas_equalTo(currentView.mas_width).multipliedBy(0.33).offset(-tempF);
        make.height.mas_equalTo(imgV1.mas_width);
    }];
    
    [imgV2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(imgV1.mas_right).offset(fondListImageSpace);
        make.top.mas_equalTo(imgV1.mas_top);
        make.size.mas_equalTo(imgV1);
    }];
    
    // 第二排 给imgV3、imgV4赋值
    [imgV3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(imgV1.mas_left);
        make.top.mas_equalTo(imgV1.mas_bottom).offset(fondListImageSpace);
        make.size.mas_equalTo(imgV1);
        make.bottom.offset(0);
    }];
    [imgV4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(imgV3.mas_right).offset(fondListImageSpace);
        make.top.mas_equalTo(imgV3.mas_top);
        make.size.mas_equalTo(imgV1);
    }];
    
}
#pragma mark 6个图片
+ (void)sixImageTypography:(NSArray *)array imgV1:(UIImageView *)imgV1 {
    NSMutableArray *temp1Array = [NSMutableArray array];
    NSMutableArray *temp2Array = [NSMutableArray array];
    
    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (idx < 3) {
            [temp1Array addObject:obj];
        } else {
            [temp2Array addObject:obj];
        }
        //        *stop = YES;
    }];
    
    // 第一排
    [temp1Array mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:fondListImageSpace leadSpacing:0.f tailSpacing:0.f];
    [temp1Array mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(imgV1.mas_width);
        make.top.offset(0);
    }];
    
    // 第二排
    [temp2Array mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:fondListImageSpace leadSpacing:0.f tailSpacing:0.f];
    [temp2Array mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(imgV1.mas_width);
        make.top.mas_equalTo(imgV1.mas_bottom).offset(fondListImageSpace);
        make.bottom.offset(0);
    }];
}
#pragma mark 5个图片
+ (void)fiveImageTypography:(NSArray *)array imgV1:(UIImageView *)imgV1 imgV2:(UIImageView *)imgV2 {
    NSMutableArray *temp1Array = [NSMutableArray array];
    NSMutableArray *temp2Array = [NSMutableArray array];
    
    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (idx < 3) {
            [temp1Array addObject:obj];
        } else {
            [temp2Array addObject:obj];
        }
        //        *stop = YES;
    }];
    
    // 前3个
    [temp1Array mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:fondListImageSpace leadSpacing:0.f tailSpacing:0.f];
    [temp1Array mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(imgV1.mas_width);
        make.top.offset(0);
    }];
    
    // 后两个
    [temp2Array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIImageView *imgV = (UIImageView *)obj;
        [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
            if (idx == 0) {
                make.left.mas_equalTo(imgV1.mas_left);
                make.bottom.offset(0);
            } else {
                make.left.mas_equalTo(imgV2.mas_left);
            }
            make.top.mas_equalTo(imgV1.mas_bottom).offset(fondListImageSpace);
            make.size.mas_equalTo(imgV1);
        }];
    }];
}

#pragma mark 7/8/9个图片
+ (void)sevenImageTypography:(NSArray *)array imgV1:(UIImageView *)imgV1 imgV2:(UIImageView *)imgV2 imgV3:(UIImageView *)imgV3 imgV4:(UIImageView *)imgV4 {
    NSMutableArray *temp1Array = [NSMutableArray array];
    NSMutableArray *temp2Array = [NSMutableArray array];
    NSMutableArray *temp3Array = [NSMutableArray array];
    
    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (idx < 3) {
            [temp1Array addObject:obj];
        } else if (idx < 6) {
            [temp2Array addObject:obj];
        } else {
            [temp3Array addObject:obj];
        }
        //        *stop = YES;
    }];
    
    // 前3个
    [temp1Array mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:fondListImageSpace leadSpacing:0.f tailSpacing:0.f];
    [temp1Array mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(imgV1.mas_width);
        make.top.offset(0);
    }];
    // 中间3个
    [temp2Array mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:fondListImageSpace leadSpacing:0.f tailSpacing:0.f];
    [temp2Array mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(imgV1.mas_width);
        make.top.mas_equalTo(imgV1.mas_bottom).offset(fondListImageSpace);
    }];
    
    // 后一两个
    [temp3Array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIImageView *imgV = (UIImageView *)obj;
        [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
            if (idx == 0) {
                make.left.mas_equalTo(imgV1.mas_left);
                make.bottom.offset(0);
            } else if (idx == 1){
                make.left.mas_equalTo(imgV2.mas_left);
            } else {
                make.left.mas_equalTo(imgV3.mas_left);
            }
            make.top.mas_equalTo(imgV4.mas_bottom).offset(fondListImageSpace);
            make.size.mas_equalTo(imgV1);
        }];
    }];
}


@end
