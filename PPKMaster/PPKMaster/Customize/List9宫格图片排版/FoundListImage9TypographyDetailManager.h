//
//  FoundListImage9TypographyDetailManager.h
//  LePin
//
//  Created by  ebz on 2020/7/30.
//  Copyright © 2020 ebz. All rights reserved.
//

#import <Foundation/Foundation.h>
static CGFloat const imgHeight = 166;

NS_ASSUME_NONNULL_BEGIN

@interface FoundListImage9TypographyDetailManager : NSObject

#pragma mark 赋值
+ (void)setValeForImageView:(NSArray *)array imageArray:(NSArray *)imageArray;

#pragma mark 0个图片
+ (void)zoreImageTypographyImgV1:(UIImageView *)imgV1 currentView:(UIView *)currentView;
#pragma mark 1个图片
+ (void)oneImageTypographyImgV1:(UIImageView *)imgV1 currentView:(UIView *)currentView;
#pragma mark 2个图片
+ (void)twoImageTypography:(NSArray *)array imgV1:(UIImageView *)imgV1 imgV2:(UIImageView *)imgV2 currentView:(UIView *)currentView;
#pragma mark 3个图片
+ (void)threeImageTypography:(NSArray *)array imgV1:(UIImageView *)imgV1;
#pragma mark 4个图片
+ (void)fourImageTypography:(NSArray *)array imgV1:(UIImageView *)imgV1 imgV2:(UIImageView *)imgV2 imgV3:(UIImageView *)imgV3 imgV4:(UIImageView *)imgV4 currentView:(UIView *)currentView;
#pragma mark 5个图片
+ (void)fiveImageTypography:(NSArray *)array imgV1:(UIImageView *)imgV1 imgV2:(UIImageView *)imgV2;
#pragma mark 6个图片
+ (void)sixImageTypography:(NSArray *)array imgV1:(UIImageView *)imgV1;
#pragma mark 7/8/9个图片
+ (void)sevenImageTypography:(NSArray *)array imgV1:(UIImageView *)imgV1 imgV2:(UIImageView *)imgV2 imgV3:(UIImageView *)imgV3 imgV4:(UIImageView *)imgV4;

@end

NS_ASSUME_NONNULL_END
