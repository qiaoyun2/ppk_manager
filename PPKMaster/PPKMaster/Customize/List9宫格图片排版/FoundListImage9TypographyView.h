//
//  FoundListImage9TypographyView.h
//  LePin
//
//  Created by  ebz on 2020/7/30.
//  Copyright © 2020 ebz. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^FoundListImage9TypographyViewClickImageVBlock)(NSInteger index);

NS_ASSUME_NONNULL_BEGIN

@interface FoundListImage9TypographyView : UIView

@property (nonatomic, assign) BOOL zoreImgB; // 1：0张图片处理
@property (nonatomic, strong) NSArray *imageArray;
@property (nonatomic, strong) NSString *videoPath;

@property (nonatomic, copy) FoundListImage9TypographyViewClickImageVBlock clickImageVCallBack;

//@property (nonatomic, strong) UIImageView *getImageV; //
@property (nonatomic, assign ) IBInspectable BOOL is_tap; // 1：可以点击图片直接放大，不用走代理方法 0：点击图片直接走代理

- (UIImageView *)getImageV:(NSInteger)index; // 获取当前的imgV

@end

NS_ASSUME_NONNULL_END
