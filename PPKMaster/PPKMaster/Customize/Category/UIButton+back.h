//
//  UIButton+back.h
//  ZhongRenJuMei
//
//  Created by null on 2019/4/10.
//  Copyright © 2019 null. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIButton (back)
// 添加渐变色
- (void)setBackgroundDefaultGradientColor;

@end

NS_ASSUME_NONNULL_END
