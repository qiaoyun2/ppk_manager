//
//  UILabel+Category.m
//  JKHousekeeper
//
//  Created by null on 2020/1/8.
//  Copyright © 2020 Lingday. All rights reserved.
//

#import "UILabel+Category.h"
@implementation UILabel (Category)

/// 中划线
/// @param text 字符
/// @param color 颜色
-(void)setMiddlelineStyleSingle:(NSString *)text color:(UIColor *)color
{
     //添加中划线
     NSDictionary * centerAttribtDic = @{NSStrikethroughStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle],NSForegroundColorAttributeName:color};
     NSMutableAttributedString * centerAttr = [[NSMutableAttributedString alloc] initWithString:text attributes:centerAttribtDic];
     self.attributedText = centerAttr;
}

/// 下划线
/// @param text 字符
/// @param color 颜色
-(void)setUnderlineStyleSingle:(NSString *)text color:(UIColor *)color
{
     //添加下划线
     NSDictionary * underAttribtDic  = @{NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle],NSForegroundColorAttributeName:color};
     NSMutableAttributedString * underAttr = [[NSMutableAttributedString alloc] initWithString:text attributes:underAttribtDic];
     self.attributedText = underAttr;
}
@end
