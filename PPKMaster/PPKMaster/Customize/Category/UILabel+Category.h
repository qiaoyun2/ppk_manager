//
//  UILabel+Category.h
//  JKHousekeeper
//
//  Created by null on 2020/1/8.
//  Copyright © 2020 Lingday. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UILabel (Category)
/// 中划线
/// @param text 字符
/// @param color 颜色
-(void)setMiddlelineStyleSingle:(NSString *)text color:(UIColor *)color;
/// 下划线
/// @param text 字符
/// @param color 颜色
-(void)setUnderlineStyleSingle:(NSString *)text color:(UIColor *)color;
@end

NS_ASSUME_NONNULL_END
