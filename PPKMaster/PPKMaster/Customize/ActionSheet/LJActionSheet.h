//
//  LJActionSheet.h
//  wash
//
//  Created by weixikeji on 15/5/11.
//
//

#import <UIKit/UIKit.h>

@class LJActionSheet;

@protocol LJActionSheetDelegate <NSObject>

@optional
 
/**
 *  点击按钮
 */
- (void)actionSheet:(LJActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex;

@end

@interface LJActionSheet : UIView

/**
 *  代理
 */
@property (nonatomic, weak) id <LJActionSheetDelegate> delegate;

/**
 *  创建对象方法
 */
- (instancetype)initWithDelegate:(id<LJActionSheetDelegate>)delegate CancelTitle:(NSString *)cancelTitle OtherTitles:(NSArray*)otherTitles,... NS_REQUIRES_NIL_TERMINATION;

- (void)show;

@end
