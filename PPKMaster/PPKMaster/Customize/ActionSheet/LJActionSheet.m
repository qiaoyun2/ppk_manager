//
//  LJActionSheet.m
//  wash
//
//  Created by weixikeji on 15/5/11.
//
//

#import "LJActionSheet.h"

// 每个按钮的高度
#define OTHERS_HEIGHT  46
#define CANCEL_HEIGHT  (46 + bottomBarH)

// 取消按钮上面的间隔高度
#define Margin 8

// 背景色
#define GlobelBgColor RGB(237, 240, 242)
// 分割线颜色
#define GlobelSeparatorColor RGB(226, 226, 226)
// 字体
#define HeitiLight(f) [UIFont fontWithName:@"STHeitiSC-Light" size:f]

@interface LJActionSheet ()
{
    int _tag;
}

@property (nonatomic, weak) LJActionSheet *actionSheet;
@property (nonatomic, weak) UIView *sheetView;

@end

@implementation LJActionSheet

- (instancetype)initWithDelegate:(id<LJActionSheetDelegate>)delegate CancelTitle:(NSString *)cancelTitle OtherTitles:(NSArray *)otherTitles, ...
{
    LJActionSheet *actionSheet = [self init];
    self.actionSheet = actionSheet;
    
    actionSheet.delegate = delegate;
    
    // 黑色遮盖
    actionSheet.frame = [UIScreen mainScreen].bounds;
    actionSheet.backgroundColor = [UIColor blackColor];
    [[UIApplication sharedApplication].keyWindow addSubview:actionSheet];
    actionSheet.alpha = 0.0;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(coverClick)];
    [actionSheet addGestureRecognizer:tap];
    
    // sheet
    UIView *sheetView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0)];
    sheetView.backgroundColor = GlobelBgColor;
    sheetView.alpha = 1;
    [[UIApplication sharedApplication].keyWindow addSubview:sheetView];
    self.sheetView = sheetView;
    sheetView.hidden = YES;
    _tag = 0;
    
    for (NSString *str in otherTitles) {
        [self setupBtnWithTitle:str];

    }

    CGRect sheetViewF = sheetView.frame;
    sheetViewF.size.height = OTHERS_HEIGHT * _tag + Margin +CANCEL_HEIGHT;
    sheetView.frame = sheetViewF;
    
    // 取消按钮
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, sheetView.frame.size.height - CANCEL_HEIGHT,SCREEN_WIDTH, CANCEL_HEIGHT)];
    [btn setBackgroundImage:[self createImageWithColor:RGB(255, 255, 255)] forState:UIControlStateNormal];
    [btn setBackgroundImage:[self createImageWithColor:RGB(242, 242, 242)] forState:UIControlStateHighlighted];
//    [btn setTitle:cancelTitle forState:UIControlStateNormal];
//    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    btn.titleLabel.font = HeitiLight(17);
    btn.tag = 0;
    [btn addTarget:self action:@selector(sheetBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *cancelLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, OTHERS_HEIGHT)];
    cancelLabel.text = @"取消";
    cancelLabel.font = HeitiLight(17);
    cancelLabel.textAlignment = NSTextAlignmentCenter;
    cancelLabel.textColor = [UIColor blackColor];
    [btn addSubview:cancelLabel];
    [sheetView addSubview:btn];
    
    return actionSheet;
}

- (void)show{
    self.sheetView.hidden = NO;

    CGRect sheetViewF = self.sheetView.frame;
    sheetViewF.origin.y = SCREEN_HEIGHT;
    self.sheetView.frame = sheetViewF;
    
    CGRect newSheetViewF = self.sheetView.frame;
    newSheetViewF.origin.y = SCREEN_HEIGHT - self.sheetView.frame.size.height;
    
    [UIView animateWithDuration:0.2 animations:^{

        self.sheetView.frame = newSheetViewF;
        
        self.actionSheet.alpha = 0.3;
    }];
}

- (void)setupBtnWithTitle:(NSString *)title{
    // 创建按钮
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, OTHERS_HEIGHT *_tag , SCREEN_WIDTH, OTHERS_HEIGHT)];
    [btn setBackgroundImage:[self createImageWithColor:RGB(255, 255, 255)] forState:UIControlStateNormal];
    [btn setBackgroundImage:[self createImageWithColor:RGB(242, 242, 242)] forState:UIControlStateHighlighted];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn.titleLabel.font = HeitiLight(17);
    [btn addTarget:self action:@selector(sheetBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.sheetView addSubview:btn];
    
    // 最上面画分割线
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0.5)];
    line.backgroundColor = GlobelSeparatorColor;
    [btn addSubview:line];
    _tag ++;
    btn.tag = _tag;

}

- (void)coverClick{
    CGRect sheetViewF = self.sheetView.frame;
    sheetViewF.origin.y = SCREEN_HEIGHT;
    
    [UIView animateWithDuration:0.2 animations:^{
        self.sheetView.frame = sheetViewF;
        self.actionSheet.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self.actionSheet removeFromSuperview];
        [self.sheetView removeFromSuperview];
    }];
}

- (void)sheetBtnClick:(UIButton *)btn{
    if (btn.tag == 0) {
        [self coverClick];
        return;
    }
    
    if ([self.delegate respondsToSelector:@selector(actionSheet:clickedButtonAtIndex:)]) {
        [self.delegate actionSheet:self.actionSheet clickedButtonAtIndex:btn.tag];
        [self coverClick];
    }
}

- (UIImage*)createImageWithColor:(UIColor*)color
{
    CGRect rect=CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return theImage;
}

@end
