//
//  SignInView.m
//  ZZR
//
//  Created by null on 2021/9/27.
//

#import "SignInView.h"
#import "XBCalendar.h"
#import "XBCalendarMonthModePresenter.h"
#import "NSDate+Category.h"

@interface SignInView ()<XBCalendarDelegate>
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
///内容
@property (weak, nonatomic) IBOutlet UIView *contetView;
///日历背景view
@property (weak, nonatomic) IBOutlet UIView *calendarBgView;
///XBCalendar
@property (nonatomic,strong) XBCalendar *calendar;
///block
@property (nonatomic, copy) void(^Block)(NSString *start,NSString *end);
@end
@implementation SignInView

static dispatch_once_t _token;

- (void)awakeFromNib {
    [super awakeFromNib];
    self.calendar = [[XBCalendar alloc] initWithFirstDay:XBCalendarWeekFirstDay_sun delegate:self];
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    // 获取当前日期
    NSDate* dt = [NSDate date];
    // 定义一个时间字段的旗标，指定将会获取指定年、月、日、时、分、秒的信息
    unsigned unitFlags = NSCalendarUnitYear |
    NSCalendarUnitMonth |  NSCalendarUnitDay |
    NSCalendarUnitHour |  NSCalendarUnitMinute |
    NSCalendarUnitSecond | NSCalendarUnitWeekday;
    // 获取不同时间字段的信息
    NSDateComponents* comp = [gregorian components: unitFlags
                                          fromDate:dt];
    self.dateLabel.text = [NSString stringWithFormat:@"%ld-%02ld",comp.year,comp.month];
    
    [self.calendarBgView addSubview:self.calendar];
    [self.calendar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.calendarBgView);
        make.leading.trailing.equalTo(self.calendarBgView);
    }];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(calendarSelected:) name:kNotice_calendarSelected object:nil];
    
}
#pragma mark - 日历的选择
- (void)calendarSelected:(NSNotification *)noti{
    
    if (self.Block) {
        self.Block([NSDate stringFromDate:self.calendar.monthPresenter.selectedStartDate format:@"yyyy-MM-dd"], [NSDate stringFromDate:self.calendar.monthPresenter.selectedEndDate format:@"yyyy-MM-dd"]);
    }
    self.calendar.userInteractionEnabled = NO;
    // 第一个参数:延迟的时间
    // 可以通过改变队列来改变线程
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        // 需要延迟执行的代码
        [self dismiss];
    });
    
}
static SignInView *manager = nil;
+(instancetype)shareManager
{
    dispatch_once(&_token, ^{
        manager = [[[NSBundle mainBundle] loadNibNamed:@"SignInView" owner:self options:nil] lastObject];
    });
    return manager;
}

/// 重新初始化
+ (void)reinitialize {
    _token = 0;
}

#pragma mark - 展示
-(void)show
{
    self.hidden = NO;
    self.calendar.userInteractionEnabled = YES;
    CGAffineTransform transform = CGAffineTransformScale(CGAffineTransformIdentity,1.0,1.0);
    self.contetView.transform = CGAffineTransformScale(CGAffineTransformIdentity,0.2,0.2);
    self.contetView.alpha = 0;
    self.frame=CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [UIView animateWithDuration:0.3 delay:0.1 usingSpringWithDamping:0.5 initialSpringVelocity:10 options:UIViewAnimationOptionCurveLinear animations:^{
        self.contetView.transform = transform;
        self.contetView.alpha = 1;
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.3f];
        
    } completion:^(BOOL finished) {
    }];
}
#pragma mark - 隐藏
-(void)dismiss
{
    [UIView animateWithDuration:0.3 animations:^{
        self.contetView.transform=CGAffineTransformMakeScale(0.02, 0.02);
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark - 签到
+(SignInView *)title:(NSString *)title show:(void(^)(NSString *start,NSString *end))block{
    SignInView *view = [SignInView shareManager];
    view.Block = block;
    [view show];
    return view;
}
#pragma mark - 月的选择
- (IBAction)rightMonethClick:(UIButton *)sender {//1，2
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    // 获取当前日期
    NSDate* dt = [self.calendar currentDate];
    // 定义一个时间字段的旗标，指定将会获取指定年、月、日、时、分、秒的信息
    unsigned unitFlags = NSCalendarUnitYear |
    NSCalendarUnitMonth |  NSCalendarUnitDay |
    NSCalendarUnitHour |  NSCalendarUnitMinute |
    NSCalendarUnitSecond | NSCalendarUnitWeekday;
    // 获取不同时间字段的信息
    NSDateComponents* comp = [gregorian components: unitFlags
                                          fromDate:dt];
    NSInteger month = comp.month;
    NSInteger year = comp.year;
    if (sender.tag == 1) {
        if (month == 1) {
            month = 12;
            year = year - 1;
        }else{
            month = month - 1;
        }
        self.dateLabel.text = [NSString stringWithFormat:@"%ld-%02ld",year,month];
        [self.calendar.monthPresenter showYear:year month:month];
    }else{
        if (month == 12) {
            month = 1;
            year = year + 1;
        }else{
            month = month + 1;
        }
        self.dateLabel.text = [NSString stringWithFormat:@"%ld-%02ld",year,month];
        [self.calendar.monthPresenter showYear:year month:month];
    }
}
#pragma mark - 取消
- (IBAction)cancelBtnClick:(id)sender {
    [self dismiss];
}

#pragma mark - 年的选择
- (IBAction)yearBtnClick:(UIButton *)sender {//3，4
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    // 获取当前日期
    NSDate* dt = [self.calendar currentDate];
    // 定义一个时间字段的旗标，指定将会获取指定年、月、日、时、分、秒的信息
    unsigned unitFlags = NSCalendarUnitYear |
    NSCalendarUnitMonth |  NSCalendarUnitDay |
    NSCalendarUnitHour |  NSCalendarUnitMinute |
    NSCalendarUnitSecond | NSCalendarUnitWeekday;
    // 获取不同时间字段的信息
    NSDateComponents* comp = [gregorian components: unitFlags
                                          fromDate:dt];
    NSInteger month = comp.month;
    NSInteger year = comp.year;
    if (sender.tag == 3) {
        year = year - 1;
        self.dateLabel.text = [NSString stringWithFormat:@"%ld-%02ld",year,month];
        [self.calendar.monthPresenter showYear:year month:month];
    }else{
        year = year + 1;
        self.dateLabel.text = [NSString stringWithFormat:@"%ld-%02ld",year,month];
        [self.calendar.monthPresenter showYear:year month:month];
    }
}
- (void)dealloc
{
    NSLog(@"=====> %@ dealloc");
}

@end
