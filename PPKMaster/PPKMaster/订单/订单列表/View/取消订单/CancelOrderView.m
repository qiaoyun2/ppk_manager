//
//  CancelOrderView.m
//  PPKMaster
//
//  Created by null on 2022/4/23.
//

#import "CancelOrderView.h"
#import "CancelReasonCell.h"

@interface CancelOrderView ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeight;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, assign) NSInteger index;

@end

@implementation CancelOrderView

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.hidden = YES;
    self.contentViewHeight.constant = 380+bottomBarH;
    self.contentViewBottom.constant = - self.contentViewHeight.constant;
    [self.contentView addRoundedCorners:UIRectCornerTopLeft|UIRectCornerTopRight withRadii:CGSizeMake(12, 12) viewRect:CGRectMake(0, 0, SCREEN_WIDTH, self.contentViewHeight.constant)];
    self.index = 0;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self requestForReason];
}

- (void)requestForReason
{
    [NetworkingTool getWithUrl:kRWCancelReasonURL params:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            NSArray *dataArray = responseObject[@"data"];
            self.dataArray = [NSMutableArray arrayWithArray:dataArray];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [self.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

#pragma mark - TableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CancelReasonCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CancelReasonCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"CancelReasonCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }

    if (indexPath.row == self.index) {
        cell.selectedButton.selected = YES;
    }else {
        cell.selectedButton.selected = NO;
    }
    if (self.dataArray.count) {
        NSDictionary *dic = self.dataArray[indexPath.row];
        cell.reasonLabel.text = dic[@"content"];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.index = indexPath.row;
    [self.tableView reloadData];
}

#pragma mark - Function

- (void)show
{
    self.hidden = NO;
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.2 animations:^{
        self.backgroundColor = RGBA(0, 0, 0, 0.4);
        self.contentViewBottom.constant = 0;
        [self layoutIfNeeded];
    }];
}

- (void)dismiss
{
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.2 animations:^{
        self.backgroundColor = RGBA(0, 0, 0, 0);
        self.contentViewBottom.constant = - self.contentViewHeight.constant;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.hidden = YES;
    }];
}

- (IBAction)confirmButtonClick:(UIButton *)sender {
    if (self.dataArray.count) {
        NSDictionary *dic = self.dataArray[self.index];
        if (self.didSelectBlock) {
            self.didSelectBlock(dic[@"content"], [NSString stringWithFormat:@"%@",dic[@"id"]]);
        }
    }else {
        [LJTools showText:@"请选择取消原因" delay:1.5];
    }
    [self dismiss];
}

- (IBAction)closeButtonClick:(id)sender {
    [self dismiss];
}

@end
