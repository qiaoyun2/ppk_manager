//
//  CancelOrderView.h
//  PPKMaster
//
//  Created by null on 2022/4/23.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CancelOrderView : UIView

@property (nonatomic, copy) void(^didSelectBlock)(NSString *selectedStr, NSString *ID);/**<选择的返回类型回调 */

- (void)show;
- (void)dismiss;

@end

NS_ASSUME_NONNULL_END
