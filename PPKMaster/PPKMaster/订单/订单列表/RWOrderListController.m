//
//  RWOrderListController.m
//  PPK
//
//  Created by null on 2022/4/12.
//

#import "RWOrderListController.h"
#import "RWOrderListCell.h"
#import "RWOrderDetailController.h"
//#import "RWEvaluationController.h"
#import "ConfirmAmountView.h"
#import "PayViewController.h"
#import "CancelOrderView.h"
#import "FinishOrderController.h"

@interface RWOrderListController () <UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, assign) NSInteger page;

@property (nonatomic, strong) CancelOrderView *reasonView;

@end

@implementation RWOrderListController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self refresh];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.navigationController.interactivePopGestureRecognizer.enabled = false;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xF5F6F9);
    WeakSelf
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^(void) {
        [weakSelf refresh];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMore];
    }];
}

#pragma mark - Network
- (void)refresh {
    _page = 1;
    [self getDataArrayFromServerIsRefresh:YES];
}
- (void)loadMore {
    _page++;
    [self getDataArrayFromServerIsRefresh:NO];
}

- (void)getDataArrayFromServerIsRefresh:(BOOL)isRefresh {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
    params[@"pageNo"] = @(_page);
    params[@"pageSize"] = @(10);
    params[@"status"] = @(self.status);
    WeakSelf
    [NetworkingTool getWithUrl:kRWOrderListURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            if (isRefresh) {
                [self.dataArray removeAllObjects];
            }
            NSDictionary *dataDic = responseObject[@"data"];
            NSArray *dataArray = dataDic[@"records"];
            for (NSDictionary *obj in dataArray) {
                RWOrderListModel *model = [[RWOrderListModel alloc] initWithDictionary:obj];
                [self.dataArray addObject:model];
            }
        }
        else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [self addBlankOnView:self.tableView];
        self.noDataView.hidden = weakSelf.dataArray.count != 0;
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [self.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}

- (void)requestForCancelOrder:(RWOrderListModel *)model reasonId:(NSString *)ID reasonStr:(NSString *)reason
{
    [NetworkingTool postWithUrl:kRWCancelOrderURL params:@{@"orderId":model.orderId, @"cancelId":ID,@"cancelRemark":reason} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:@"取消成功" delay:1.5];
            [self.dataArray removeObject:model];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self refresh];
            });
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

- (void)requestForDeleteOrder:(RWOrderListModel *)model
{
    [NetworkingTool postWithUrl:kRWDeleteOrderURL params:@{@"orderId":model.orderId} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:@"删除成功" delay:1.5];
            [self.dataArray removeObject:model];
            [self.tableView reloadData];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

- (void)requestForFinishOrder:(RWOrderListModel *)model
{
    [NetworkingTool postWithUrl:kRWCompleteOrderURL params:@{@"orderId":model.orderId} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [self refresh];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

//- (void)requestForCom

#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RWOrderListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RWOrderListCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"RWOrderListCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    RWOrderListModel *model = self.dataArray[indexPath.row];
    cell.model = model;
    WeakSelf
    [cell setOnButtonsClick:^(NSInteger tag) {
        /// 0:取消  1:完成   3:删除
        if (tag==0) {
            [weakSelf showCancelReasonView:model];
        }
        else if (tag==1) {
            FinishOrderController *vc = [[FinishOrderController alloc] init];
            vc.orderId = model.orderId;
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }
        else {
            [UIAlertController alertViewNormalWithTitle:@"提示" message:@"确认删除订单吗？" titlesArry:@[@"确认"] indexBlock:^(NSInteger index, id obj) {
                [weakSelf requestForDeleteOrder:model];
            } okColor:MainColor cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
        }
    }];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    RWOrderListModel *model = self.dataArray[indexPath.row];
    RWOrderDetailController *vc = [[RWOrderDetailController alloc] init];
    vc.orderId = model.orderId;
    [self.navigationController pushViewController:vc animated:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = UIColorFromRGB(0xF5F6F9);
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = UIColorFromRGB(0xF5F6F9);
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 24;
}

#pragma mark - Function
//- (void)showConfirmAmountView
//{
//    ConfirmAmountView *view = [[[NSBundle mainBundle] loadNibNamed:@"ConfirmAmountView" owner:nil options:nil] firstObject];
//    view.frame = [UIScreen mainScreen].bounds;
//    [[UIApplication sharedApplication].keyWindow addSubview:view];
//    [view showPopView];
//    [view setOnCofirmAmount:^(NSString * _Nonnull money) {
//        PayViewController *vc = [[PayViewController alloc] init];
//    }];
//}

- (void)showCancelReasonView:(RWOrderListModel *)model
{
    if (!self.reasonView) {
        self.reasonView = [[[NSBundle mainBundle] loadNibNamed:@"CancelOrderView" owner:nil options:nil] firstObject];
        self.reasonView.frame = [UIScreen mainScreen].bounds;
        [[UIApplication sharedApplication].keyWindow addSubview:self.reasonView];
    }
    [self.reasonView show];
    WeakSelf
    [self.reasonView setDidSelectBlock:^(NSString * _Nonnull selectedStr, NSString * _Nonnull ID) {
        [weakSelf requestForCancelOrder:model reasonId:ID reasonStr:selectedStr];
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
