//
//  RWOrderCommentModel.h
//  PPKMaster
//
//  Created by null on 2022/4/24.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface RWOrderCommentModel : BaseModel

@property (nonatomic, strong) NSString *avatar;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSString *createTime;
@property (nonatomic, strong) NSString *isAnonymous;
@property (nonatomic, strong) NSString *nickname;
@property (nonatomic, strong) NSString *orderId;
@property (nonatomic, strong) NSString *picture;
@property (nonatomic, strong) NSString *resourceType;
@property (nonatomic, strong) NSString *starNum;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *video;
@property (nonatomic, strong) NSString *videoPicture;


@end

NS_ASSUME_NONNULL_END
