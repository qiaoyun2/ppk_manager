//
//  RWOrderDetailModel.h
//  PPK
//
//  Created by null on 2022/4/20.
//

#import "BaseModel.h"
#import "RWOrderCommentModel.h"

NS_ASSUME_NONNULL_BEGIN

//@interface RWOrderCommentModel : BaseModel
//
//@end

@interface RWOrderDetailModel : BaseModel

@property (nonatomic, strong) NSString *area; //
@property (nonatomic, strong) NSString *cancelContent;
@property (nonatomic, strong) NSString *city; //
@property (nonatomic, strong) NSString *completeTime; // 完成时间
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *firstClassify; // 电子产品
@property (nonatomic, strong) NSString *goodsNum; // 物品数量
@property (nonatomic, strong) NSString *goodsWeight; // 物品重量
@property (nonatomic, strong) NSString *ID; // 数据id
@property (nonatomic, strong) NSString *latitude;
@property (nonatomic, strong) NSString *longitude;
@property (nonatomic, strong) NSString *orderId; // 订单编号
@property (nonatomic, strong) NSString *picture; //
@property (nonatomic, strong) NSString *province; //
@property (nonatomic, strong) NSString *receiveTime; // 接单时间
@property (nonatomic, strong) NSString *receiver; // 收件人
@property (nonatomic, strong) NSString *remark; // 订单备注
@property (nonatomic, strong) NSString *reserveTime; // 预约时间段
@property (nonatomic, strong) NSString *resourceType; // 资源类型：1-图片；2-视频
@property (nonatomic, strong) NSString *secondClassify; // 笔记本电脑
@property (nonatomic, strong) NSString *status; // 订单状态：1-预约中；2-待回收；3-已完成；4-用户已取消
@property (nonatomic, strong) NSString *street; //
@property (nonatomic, strong) NSString *telphone; //
@property (nonatomic, strong) NSString *video; // 视频地址
@property (nonatomic, strong) NSString *videoPicture; // 视频封面
@property (nonatomic, strong) NSString *createTime; // 下单时间
@property (nonatomic, strong) RWOrderCommentModel *commentModel;

@property (nonatomic, strong) NSString *cancelTime; // 剩余可取消时间，单位：秒
//@property (nonatomic, strong) NSString *firstClassifyId; //
//@property (nonatomic, strong) NSString *secondClassifyId; //
//@property (nonatomic, strong) NSString *gender; // 性别：0-女；1-男
//@property (nonatomic, strong) NSString *isComment; // 本人是否评价此订单0否1是
//@property (nonatomic, strong) NSString *payTimer;
//@property (nonatomic, strong) NSString *receiveUserName; // 回收员姓名
//@property (nonatomic, strong) NSString *receiveUserTelPhone; // 回收员电
//@property (nonatomic, strong) NSString *addressId;
//@property (nonatomic, strong) NSString *distance;

@end

NS_ASSUME_NONNULL_END
