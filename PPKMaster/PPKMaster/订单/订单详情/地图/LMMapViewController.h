//
//  LMMapViewController.h
//  PPK
//
//  Created by null on 2022/3/17.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LMMapViewController : BaseViewController

@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *district;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *distance;

@property (nonatomic) CLLocationCoordinate2D coordinate;

@end

NS_ASSUME_NONNULL_END
