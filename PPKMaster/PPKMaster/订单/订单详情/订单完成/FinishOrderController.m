//
//  FinishOrderController.m
//  PPKMaster
//
//  Created by null on 2022/5/5.
//

#import "FinishOrderController.h"
#import <HXPhotoPicker/HXPhotoPicker.h>
#import "HXAssetManager.h"
#import "UploadManager.h"

@interface FinishOrderController () <HXPhotoViewDelegate>
@property (weak, nonatomic) IBOutlet HXPhotoView *photoView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *photoViewHeight;

@property (nonatomic, strong) NSMutableArray<UIImage *> *photos;
@property (nonatomic, strong) NSMutableArray *imagePathArray;
@property (strong, nonatomic) HXPhotoManager *manager;

@end

@implementation FinishOrderController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"订单完成";

    self.photos = [NSMutableArray array];
    self.imagePathArray = [NSMutableArray array];
    self.photoView.spacing = 12.f;
    self.photoView.delegate = self;
    self.photoView.deleteCellShowAlert = YES;
    self.photoView.outerCamera = YES;
    self.photoView.previewShowDeleteButton = YES;
    self.photoView.addImageName = @"组 50936";
    self.photoView.lineCount = 3;
    self.photoView.manager = self.manager;
}

#pragma mark - Network
- (void)requestForUploadImages
{
    WeakSelf
    [UploadManager uploadImageArray:self.photos block:^(NSString * _Nonnull ids, NSString * _Nonnull imageUrl) {
        NSArray *array = [imageUrl componentsSeparatedByString:@","];
        [weakSelf.imagePathArray addObjectsFromArray:array];
        [weakSelf requestForFinish];
    }];
}

- (void)requestForFinish {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:[self.imagePathArray componentsJoinedByString:@","] forKey:@"picture"];
    [params setValue:self.orderId forKey:@"orderId"];
    [NetworkingTool postWithUrl:kRWCompleteOrderURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }
        [LJTools showText:responseObject[@"msg"] delay:1.5];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

#pragma mark - HXPhotoViewDelegate
- (void)photoView:(HXPhotoView *)photoView changeComplete:(NSArray<HXPhotoModel *> *)allList photos:(NSArray<HXPhotoModel *> *)photos videos:(NSArray<HXPhotoModel *> *)videos original:(BOOL)isOriginal {
    [self.photos removeAllObjects];
    [self.imagePathArray removeAllObjects];
    for (HXPhotoModel *model in photos) {
        [self.photos addObject:[HXAssetManager originImageForAsset:model.asset]];
    }
}

// 拿到view变化的高度
- (void)photoView:(HXPhotoView *)photoView updateFrame:(CGRect)frame {
    self.photoViewHeight.constant = frame.size.height;
}

#pragma mark - Lazy Load
- (HXPhotoManager *)manager {
    if (!_manager) {
        _manager = [[HXPhotoManager alloc] initWithType:HXPhotoManagerSelectedTypePhoto];
        _manager.configuration.type = HXConfigurationTypeWXChat;
        _manager.configuration.singleSelected = NO;
        _manager.configuration.lookGifPhoto = NO;
        _manager.configuration.useWxPhotoEdit = YES;
        _manager.configuration.selectTogether=NO;
        _manager.configuration.photoEditConfigur.onlyCliping = YES;
        _manager.configuration.showOriginalBytes = NO;
        _manager.configuration.videoMaxNum=0;
        _manager.configuration.photoMaxNum=9;
        _manager.configuration.requestImageAfterFinishingSelection = YES;
        _manager.configuration.photoEditConfigur.aspectRatio = HXPhotoEditAspectRatioType_None;
    }
    return _manager;
}

#pragma mark - XibFunction
- (IBAction)finishButtonAction:(UIButton *)sender {
    if (!self.photos.count) {
        [LJTools showText:@"请上传图片" delay:1.5];
        return;
    }
    [self requestForUploadImages];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
