//
//  FinishOrderController.h
//  PPKMaster
//
//  Created by null on 2022/5/5.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FinishOrderController : BaseViewController

@property (nonatomic, strong) NSString *orderId;

@end

NS_ASSUME_NONNULL_END
