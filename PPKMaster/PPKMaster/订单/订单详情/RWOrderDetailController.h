//
//  RWOrderDetailController.h
//  PPK
//
//  Created by null on 2022/4/12.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface RWOrderDetailController : BaseViewController
@property (nonatomic, strong) NSString *orderId;

@end

NS_ASSUME_NONNULL_END
