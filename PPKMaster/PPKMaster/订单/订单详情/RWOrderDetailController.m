//
//  RWOrderDetailController.m
//  PPK
//
//  Created by null on 2022/4/12.
//

#import "RWOrderDetailController.h"
#import "FoundListImage9TypographyView.h"
#import "LMMapViewController.h"
#import "RWOrderDetailModel.h"
#import "CancelOrderView.h"
#import "GBStarRateView.h"
#import "FinishOrderController.h"

@interface RWOrderDetailController ()

/** 状态*/
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIView *unConfirmTipView; // 待回收状态提示
@property (weak, nonatomic) IBOutlet UILabel *cancelTipLabel; // 取消状态提示

@property (weak, nonatomic) IBOutlet UIView *reserveTimeView; // 预约上门时间
@property (weak, nonatomic) IBOutlet UILabel *reserveTimeLabel;

@property (weak, nonatomic) IBOutlet UIView *startTimeView; // 开始时间
@property (weak, nonatomic) IBOutlet UILabel *startTimeLabel;
@property (weak, nonatomic) IBOutlet UIView *topPayTimeView; // 付款时间
@property (weak, nonatomic) IBOutlet UILabel *topPayTimeLabel;


/** 用户评价*/
@property (weak, nonatomic) IBOutlet UIView *commentView;
@property (weak, nonatomic) IBOutlet UILabel *commentTimeLabel; // 评论时间
@property (weak, nonatomic) IBOutlet UILabel *introduceLbel; // 描述
@property (weak, nonatomic) IBOutlet FoundListImage9TypographyView *imgsView;
@property (weak, nonatomic) IBOutlet UILabel *commentNameLabel;
@property (weak, nonatomic) IBOutlet GBStarRateView *starView;


/** 取消信息*/
@property (weak, nonatomic) IBOutlet UIView *cancelView;
@property (weak, nonatomic) IBOutlet UILabel *cancelReasonLabel; // 取消原因

/** 订单详细信息*/
@property (weak, nonatomic) IBOutlet UILabel *typeLabel; // 回收类型
@property (weak, nonatomic) IBOutlet UILabel *numLabel; // 回收数量
@property (weak, nonatomic) IBOutlet UILabel *weightLabel; // 回收数量
@property (weak, nonatomic) IBOutlet UILabel *remarkLabel; // 备注
@property (weak, nonatomic) IBOutlet FoundListImage9TypographyView *orderImgsView;


/**  */
@property (weak, nonatomic) IBOutlet UILabel *nameLabel; // 姓名
@property (weak, nonatomic) IBOutlet UILabel *mobileLabel; // 联系方
@property (weak, nonatomic) IBOutlet UILabel *addOrderTimeLabel; // 接单时间
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

@property (weak, nonatomic) IBOutlet UIView *payTimeView; // 付款时间
@property (weak, nonatomic) IBOutlet UILabel *payTimeLabel;

@property (weak, nonatomic) IBOutlet UIView *cancelTimeView; // 取消时间
@property (weak, nonatomic) IBOutlet UILabel *cancelTimeLabel;

@property (weak, nonatomic) IBOutlet UIButton *cancelButton; // 取消  0
@property (weak, nonatomic) IBOutlet UIButton *finishButton; // 完成  2
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;  // 删除  3
@property (weak, nonatomic) IBOutlet UIStackView *buttonsStackView;

@property (nonatomic, strong) RWOrderDetailModel *detailModel;
@property (nonatomic, strong) CancelOrderView *reasonView;

@end

@implementation RWOrderDetailController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [self requestForOrderDetail];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"订单详情";
    for (UIButton *button in self.buttonsStackView.arrangedSubviews) {
        button.hidden = YES;
    }
    self.imgsView.imageArray = @[@"girl", @"girl2", @"girl3",@"girl", @"girl2", @"girl3",@"girl", @"girl2", @"girl3"];
    self.starView.allowClickScore = NO;
    self.starView.allowSlideScore = NO;
    self.starView.spacingBetweenStars = 2;
    self.starView.starSize = CGSizeMake(10, 10);
    self.starView.starImage = [UIImage imageNamed:@"star_off"];
    self.starView.currentStarImage = [UIImage imageNamed:@"star_on"];
    self.starView.style = GBStarRateViewStyleIncompleteStar;
    self.starView.isAnimation = NO;
}


#pragma mark - Network
- (void)requestForOrderDetail
{
    [NetworkingTool getWithUrl:kRWOrderDetailURL params:@{@"orderId":self.orderId} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            NSDictionary *dataDic = responseObject[@"data"];
            self.detailModel = [[RWOrderDetailModel alloc] initWithDictionary:dataDic];
            [self updateSubviews];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

- (void)requestForCancelOrder:(NSString *)ID reasonStr:(NSString *)reason
{
    [NetworkingTool postWithUrl:kRWCancelOrderURL params:@{@"orderId":self.orderId, @"cancelId":ID,@"cancelRemark":reason} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:@"取消成功" delay:1.5];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 *NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self requestForOrderDetail];
            });
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

- (void)requestForDeleteOrder
{
    [NetworkingTool postWithUrl:kRWDeleteOrderURL params:@{@"orderId":self.orderId} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:@"删除成功" delay:1.5];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

- (void)requestForFinishOrder
{
    [NetworkingTool postWithUrl:kRWCompleteOrderURL params:@{@"orderId":self.orderId} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [self requestForOrderDetail];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

#pragma mark - Function
- (void)updateSubviews
{
    NSInteger status = [self.detailModel.status integerValue];
    self.statusLabel.text = [LJTools getRWStatusFormat:status];
    self.reserveTimeLabel.text = _detailModel.receiveTime;
    self.startTimeLabel.text = _detailModel.receiveTime;
    self.topPayTimeLabel.text = _detailModel.completeTime;
    
    if (_detailModel.commentModel.userId.length) {
        RWOrderCommentModel *commentModel = _detailModel.commentModel;
        self.introduceLbel.text = commentModel.content;
        self.commentTimeLabel.text = commentModel.createTime;
        if (commentModel.video.length>0) {
            self.imgsView.videoPath = commentModel.video;
            self.imgsView.imageArray= @[commentModel.videoPicture];
        }
        else {
            if (commentModel.picture.length>0) {
                NSArray *images = [commentModel.picture componentsSeparatedByString:@","];
                self.imgsView.imageArray= images;
            }
        }
        self.starView.currentStarRate = [commentModel.starNum floatValue];
    }

    self.cancelReasonLabel.text = _detailModel.cancelContent;
    
    self.typeLabel.text = [NSString stringWithFormat:@"%@ %@",_detailModel.firstClassify, _detailModel.secondClassify];
    self.numLabel.text = [NSString stringWithFormat:@"%@",_detailModel.goodsNum];
    self.weightLabel.text = [NSString stringWithFormat:@"%@",_detailModel.goodsWeight];
    self.remarkLabel.text = _detailModel.remark;
    if (_detailModel.video.length>0) {
        self.orderImgsView.videoPath = _detailModel.video;
        self.orderImgsView.imageArray= @[_detailModel.videoPicture];
    }
    else {
        if (_detailModel.picture.length>0) {
            NSArray *images = [_detailModel.picture componentsSeparatedByString:@","];
            self.orderImgsView.imageArray= images;
        }
    }


    self.nameLabel.text = _detailModel.receiver;
    self.mobileLabel.text = _detailModel.telphone;
    self.addressLabel.text = [NSString stringWithFormat:@"%@%@%@%@",_detailModel.province,_detailModel.city,_detailModel.area,_detailModel.street];
    self.addOrderTimeLabel.text = _detailModel.receiveTime;
    self.payTimeLabel.text = _detailModel.receiveTime;
//    self.cancelTimeLabel.text = _detailModel;

    for (UIButton *button in self.buttonsStackView.arrangedSubviews) {
        button.hidden = YES;
    }
    
    if (status==2) {
        // 待回收
        self.cancelButton.hidden = [_detailModel.cancelTime integerValue] <= 0;
        self.finishButton.hidden = NO;
        
        self.unConfirmTipView.hidden = NO;
        self.reserveTimeView.hidden = NO;
        self.startTimeView.hidden = YES;
        self.topPayTimeView.hidden = YES;
        self.cancelTipLabel.hidden = YES;

        self.commentView.hidden = YES;

        self.cancelView.hidden = YES;
        
        self.payTimeView.hidden = YES;
        self.cancelTimeView.hidden = YES;
        
    }
    else if (status==3) {
        // 已完成
        self.deleteButton.hidden = NO;
        
        self.unConfirmTipView.hidden = YES;
        self.reserveTimeView.hidden = YES;
        self.startTimeView.hidden = NO;
        self.topPayTimeView.hidden = NO;
        self.cancelTipLabel.hidden = YES;
        
        self.commentView.hidden = !_detailModel.commentModel.userId.length;

        self.cancelView.hidden = YES;
        
        self.payTimeView.hidden = NO;
        self.cancelTimeView.hidden = YES;
    }
    else {
        // 已取消
        self.deleteButton.hidden = NO;
        
        self.unConfirmTipView.hidden = YES;
        self.reserveTimeView.hidden = YES;
        self.startTimeView.hidden = YES;
        self.topPayTimeView.hidden = YES;
        self.cancelTipLabel.hidden = NO;

        self.commentView.hidden = YES;

        self.cancelView.hidden = NO;
        
        self.payTimeView.hidden = YES;
        self.cancelTimeView.hidden = NO;
    }
}

- (void)showCancelReasonView
{
    if (!self.reasonView) {
        self.reasonView = [[[NSBundle mainBundle] loadNibNamed:@"CancelOrderView" owner:nil options:nil] firstObject];
        self.reasonView.frame = [UIScreen mainScreen].bounds;
        [[UIApplication sharedApplication].keyWindow addSubview:self.reasonView];
    }
    [self.reasonView show];
    WeakSelf
    [self.reasonView setDidSelectBlock:^(NSString * _Nonnull selectedStr, NSString * _Nonnull ID) {
        [weakSelf requestForCancelOrder:ID reasonStr:selectedStr];
    }];
}

- (IBAction)buttonsAction:(UIButton *)sender {
    NSInteger tag = sender.tag;
    /// 0:取消  1:完成   3:删除
    WeakSelf
    if (tag==0) {
        [weakSelf showCancelReasonView];
    }
    else if (tag==1) {
        FinishOrderController *vc = [[FinishOrderController alloc] init];
        vc.orderId = self.orderId;
        [weakSelf.navigationController pushViewController:vc animated:YES];
    }
    else {
        [UIAlertController alertViewNormalWithTitle:@"提示" message:@"确认删除订单吗？" titlesArry:@[@"确认"] indexBlock:^(NSInteger index, id obj) {
            [weakSelf requestForDeleteOrder];
        } okColor:MainColor cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
    }
}


- (IBAction)callButtonAction:(id)sender {
    [LJTools call:self.detailModel.telphone];
}


- (IBAction)guideViewTap:(id)sender {
    if ([self.detailModel.status integerValue]==2) {
        LMMapViewController *vc = [[LMMapViewController alloc] init];
        vc.coordinate = CLLocationCoordinate2DMake([_detailModel.latitude floatValue], [_detailModel.longitude floatValue]);
        vc.city = _detailModel.city;
        vc.district = _detailModel.area;
        vc.address = _detailModel.street;
//        vc.distance = _detailModel.di;
        [self.navigationController pushViewController:vc animated:YES];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
