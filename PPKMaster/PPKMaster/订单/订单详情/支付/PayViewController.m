//
//  PayViewController.m
//  PPK
//
//  Created by null on 2022/3/10.
//

#import "PayViewController.h"
#import "WXApiManager.h"
//#import "PublishResultController.h"
//#import "PayResultView.h"

@interface PayViewController () <WXApiManagerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;
@property (weak, nonatomic) IBOutlet UIButton *wxPayButton;
@property (weak, nonatomic) IBOutlet UIButton *aliPayButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *payButtonBottom;

@property (nonatomic, strong) UIButton *lastButton;
@property (nonatomic ,copy) NSString *order_sn;

@end

@implementation PayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xF7F8FC);
    self.payButtonBottom.constant = 28+bottomBarH;
}

#pragma mark - Network
- (void)requestForAddOrder
{
    NSDictionary * parmars = @{
        @"order_type":@"16",
//        @"order_money":self.money,
//        @"payable_money":self.money,
//        @"pay_type":bottomSelectModel.pay_type,
//        @"level_id":self.level_id,
        @"is_wholesale":@"0"
    };
    [NetworkingTool postWithUrl:@"" params:parmars success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"]intValue]==SUCCESS) {
            NSString *order_sn = responseObject[@"data"][@"order_sn"];
            self.order_sn= order_sn;
            if (self.wxPayButton.selected) {
                [self requestForWxPay];
            } else {
                [self requestForAliPay];
            }
        }else{
            [LJTools showNOHud:responseObject[@"msg"] delay:1];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {

    } IsNeedHub:YES];
}

- (void)requestForWxPay
{
     [NetworkingTool postWithUrl:@"" params:@{@"order_sn":self.order_sn} success:^(NSURLSessionDataTask *task, id responseObject) {
         if ([responseObject[@"code"] integerValue]==1) {
             NSDictionary *dataDic = responseObject[@"data"];
             [self wechatPayWithDic:dataDic];
         }else{
             [LJTools showText:responseObject[@"msg"] delay:1.5];
         }
     } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
         
     } IsNeedHub:YES];
}

- (void)requestForAliPay
{
     [NetworkingTool postWithUrl:@"" params:@{@"order_sn":self.order_sn} success:^(NSURLSessionDataTask *task, id responseObject) {
         if ([responseObject[@"code"] integerValue]==1) {
             [self alipayWithMessage:responseObject[@"data"]];
         }else{
             [LJTools showText:responseObject[@"msg"] delay:1.5];
         }
     } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
         
     } IsNeedHub:YES];
}

#pragma mark - WXApiManagerDelegate
- (void)managerDidRecvPaymentResponse:(PayResp *)response{
    if (response.errCode==0) {
        User *user = [User getUser];
        [User saveUser:user];
    }
}

#pragma mark - Function
// 唤起微信支付
- (void)wechatPayWithDic:(NSDictionary *)payDic
{
    PayReq *request = [[PayReq alloc] init];
    request.openID = payDic[@"appid"];//公众账号ID
    request.partnerId = payDic[@"partnerid"];//微信支付分配的商户号
    request.prepayId = payDic[@"prepayid"];//预支付交易会话ID
    request.nonceStr = payDic[@"noncestr"];//随机字符串
    request.timeStamp = [payDic[@"timestamp"] intValue];//时间戳
    request.package = payDic[@"package"];//扩展字段(默认 Sign=WXPay)
    request.sign = payDic[@"sign"];//签名
    [WXApiManager sharedManager].delegate=self;
    ///发起支付
    [WXApi sendReq:request completion:^(BOOL success) {
        if (success) {
//             [self paySuccessAction];
//            [LJTools showNOHud:@"充值成功" delay:1.0];

        }
        else{
            [LJTools showNOHud:@"发起支付失败" delay:1.0];
        }
    }];
}

// 唤起支付宝支付
- (void)alipayWithMessage:(NSString *)msg
{
    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"vipPay"];
  
    [[AlipaySDK defaultService] payOrder:msg fromScheme:AlipayScheme callback:^(NSDictionary *resultDic) {
        NSLog(@"-------%@", resultDic);
//        NSString *Memo = [resultDic objectForKey:@"memo"];
        NSString *resultStatus = [resultDic objectForKey:@"resultStatus"];
        if ([resultStatus isEqualToString:@"9000"])
        {
            [LJTools showText:@"充值成功" delay:1.5];
//            PaySuccessfulVC *vc=[[PaySuccessfulVC alloc] init];
//            vc.isVip=1;
//            vc.payType=@"支付宝支付";
////            [self getUserInfo];
//            [self.navigationController pushViewController:vc animated:YES];
        }else{
            [LJTools showText:@"支付失败" delay:1.5];
        }
    }];
}

#pragma mark - XibFunction
- (IBAction)wxPayButtonAction:(UIButton *)sender {
    sender.selected = YES;
    self.aliPayButton.selected = NO;
}

- (IBAction)aliPayButtonAction:(UIButton *)sender {
    sender.selected = YES;
    self.wxPayButton.selected = NO;
}

- (IBAction)bottomButtonAction:(id)sender {
    
//    PayResultView *view = [[[NSBundle mainBundle] loadNibNamed:@"PayResultView" owner:nil options:nil] firstObject];
//    view.frame = [UIScreen mainScreen].bounds;
//    [[UIApplication sharedApplication].keyWindow addSubview:view];
//    [view showPopView];
//    WeakSelf
//    [view setOnClosed:^{
//        PublishResultController *vc = [[PublishResultController alloc] init];
//        [weakSelf.navigationController pushViewController:vc animated:YES];
//    }];
//    return;
//    [self requestForAddOrder];
    
    
    
    
}


@end
